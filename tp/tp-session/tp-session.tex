\documentclass{article}
\usepackage[margin=3cm, top=2cm]{geometry}
\usepackage[french]{babel}
\usepackage{graphicx}
\usepackage{url}
\usepackage{minted}

\title{Gestion des utilisateurs avec une session}
\author{Benoit Favre}
\date{généré le \today}

\begin{document}
\maketitle

\section{Introduction}

L'objectif de ce TP est de n'autoriser la modification des données d'un site
web qu'à des utilisateurs authentifiés.  Vous allez gérer la création de
comptes, le login, et la restriction des pages nécessitant une
authentification.  Pour cela, vous utiliserez la version améliorée du site
``Cuisiner c'est bon pour la santé" qui permet de modifier les recettes de
cuisine dans la base de données.

\section{Présentation du site}

\paragraph{Modèle.}

Cette fois, les données sont conservées dans une base de données SQLite. Le script \verb+create-db.js+ permet de remplir la base à partir du contenu de \verb+data.json+.
Une recette est constituée de métadonnées comme son titre, sa description ou sa durée, ainsi que d'une liste d'ingrédients et une liste d'étapes ({\it stages}).
Dans la suite, une recette est représentée en javascript sous la forme de l'objet suivant :
\begin{minted}{js}
var recipe = {
  id: integer, // identifiant; seulement lorsque l'on lit une recette depuis la base
  title: 'text',
  description: 'text',
  duration: 'text',
  ingredients: [
    {name: 'text'},
    {name: 'text'},
    ...
  ],
  stages: [
    {description: 'text'},
    {description: 'text'},
    ...
  ]
}
\end{minted}

La base de donnée contient trois tables créées avec les requêtes suivantes :
\begin{minted}{sql}
CREATE TABLE recipe (id INTEGER PRIMARY KEY AUTOINCREMENT, title TEXT, img TEXT, 
    description TEXT, duration TEXT)
CREATE TABLE ingredient (recipe INT, rank INT, name TEXT)
CREATE TABLE stage (recipe INT, rank INT, description TEXT)
\end{minted}
La table \verb+recipe+ contient le titre, l'url de l'image, la description et la durée des recettes; la table \verb+ingredient+ contient la liste ordonnée des ingrédients, et \verb+stage+ contient la liste ordonnée des étapes. Chaque ingrédient est associé à un numéro de recette, son indice dans la liste des ingrédients (rank) et le texte qui le décrit (name). Chaque étape (stage) de la recette est associée à son indice (rank) dans la liste des étapes et sa description.

Le modèle est défini dans le fichier \verb+model.js+ et offre les fonctions suivantes :
\begin{itemize}
  \item \verb+read(id)+ : obtenir le contenu d'une recette à partir de son identifiant
  \item \verb+create(recipe)+ : sauvegarder une recette dans la base à partir de sa représentation sous forme d'objet javascript
  \item \verb+update(id, recipe)+ : modifier le contenu d'une recette identifiée par \verb+id+ dans la base, à partir de sa représentation sous forme d'objet javascript
  \item \verb+delete(id)+ : supprimer une recette à partir de son identifiant
  \item \verb+search(query, page)+ : rechercher des recettes à partir d'une requête textuelle, et obtenir une page de résultats (d'indice \verb+page+, pour des pages de 32 éléments)  
\end{itemize}
Le fichier \verb+model.js+ contient plus de détails sur ces fonctions.

\paragraph{Vues.}

Le site contient les templates suivants de vues :
\begin{itemize}
  \item \verb+views/header.html+ : entêtes en commun entre toutes les pages
  \item \verb+views/footer.html+ : pied de page de toutes les pages
  \item \verb+views/index.html+ : page principale
  \item \verb+views/search.html+ : affichage des résultats de recherche
  \item \verb+views/read.html+ : affichage du contenu d'une recette
  \item \verb+views/create.html+ : formulaire de création d'une recette
  \item \verb+views/update.html+ : formulaire de mise à jour des informations d'une recette
  \item \verb+views/delete.html+ : formulaire de confirmation d'effacement d'une recette
\end{itemize}

\paragraph{Routes.}

Les routes auxquelles répond le serveur sont les suivantes :

\begin{itemize}
  \item GET \verb+/+ : page principale du site
  \item GET \verb+/search?query=text&page=num+ : page \verb+num+ de résultats de recherche pour la requête \verb+text+
  \item GET \verb+/read/:id+ : affichage du contenu de la recette d'identifiant \verb+id+
  \item GET \verb+/create+ :  formulaire de création d'une recette
  \item GET \verb+/update/:id+ : formulaire de mise à jour des informations de la recette d'identifiant \verb+id+
  \item GET \verb+/delete/:id+ : formulaire de confirmation d'effacement de la recette d'identifiant \verb+id+
  \item POST \verb+/create+ : route de création effective d'une recette 
  \item POST \verb+/update/:id+ : route de modification effective de la recette d'identifiant \verb+id+
  \item POST \verb+/delete/:id+ : route de suppression effective de la recette d'identifiant \verb+id+
\end{itemize}

Le fichier \verb+server.js+ contient plus de détails sur le fonctionnement de ces fonctions.
Pour faire fonctionner le serveur vous devrez installer les modules manquants : \verb+express+, \verb+mustache-express+, \verb+better-sqlite3+.

\section{Travail à faire}

L'objectif est d'ajouter la gestion des utilisateurs et d'interdire aux utilisateurs non connectés d'accéder aux pages qui permettent de modifier la base.
Pour cela, vous allez devoir implémenter l'authentification des utilisateurs, la création de nouveaux utilisateurs ainsi que le contrôler les accès aux différentes routes.
Tout d'abord, assurez-vous que vous comprenez bien tous les aspects du fonctionnement du site, puis :

\begin{enumerate}
  \item Créez une table \verb+user+ dans la base SQLite avec trois champs : \verb+id+ un identifiant unique autoincrémenté, \verb+name+ le nom de l'utilisateur et \verb+password+ son mot de passe.
  \item Ajoutez manuellement un utilisateur à la base pour pouvoir effectuer des tests (vous pouvez par exemple inclure la création de la table et de l'utilisateur dans le script \verb+create-db.js+).
\end{enumerate}

\subsection{Préparation}

Tout d'abord, pour faciliter votre tâche, installez le paquet \verb+cookie-session+ avec npm. Vous pouvez y faire appel dans le serveur comme cela :
\begin{minted}{js}
const cookieSession = require('cookie-session');
app.use(cookieSession({
  secret: 'mot-de-passe-du-cookie',
}));
\end{minted}
Ce module\footnote{Documentation ici \url{https://expressjs.com/en/resources/middleware/cookie-session.html}.} permet de garder des informations de session dans un cookie crypté passé au client à chaque requête. Il faut y associer un mot de passe unique à l'application (typiquement généré aléatoirement) pour que ce cryptage soit effectif.

\subsection{Contrôle d'accès}

Le contrôle d'accès sera implémenté sous la forme d'un middleware nommé \verb+is_authenticated+ qui vérifiera que la session contient un identifiant d'utilisateur et renverra une erreur 401 (accès non autorisé) si ce n'est pas le cas. Ce middleware sera utilisé uniquement sur les routes à accès restreint. Pour rappel, on peut appliquer un middleware à une route spécifique de cette façon :
\begin{minted}{js}
function middleware(req, res, next) {
  // ...
  next();
}
app.get('/route', middleware, (req, res) => ...);
\end{minted}

\subsection{Vues conditionnelles}

Le barre du haut définie dans la vue \verb+header.html+ et la vue \verb+read.html+ contient des boutons qui permettent de créer et modifier les recettes. On souhaite maintenant ne pas afficher ces boutons si l'utilisateur n'est pas authentifié.

Pour que les templates \verb+mustache+ aient accès à l'information d'authentification, il faut créer une valeur dans le dictionnaire \verb+res.locals+, ce qui permet de conditionner le rendu à la valeur de cette variable. Il faut créer un nouveau middleware qui va mettre \verb+res.locals.authenticated+ à true si la session est valide, et copie le nom de l'utilisateur depuis la session dans \verb+res.locals.name+. Ce middleware devra être appelé à toutes les routes, ce qui peut être fait avec \verb+app.use(middleware)+.

On pourra alors écrire un template mustache de ce type :
\begin{minted}{html}
{{#authenticated}}
  <p> contenu uniquement visible si l'utilisateur est authentifié </p>
{{/authenticated}}
\end{minted}

De la même manière, on peut cacher du contenu lorsque l'utilisateur est authentifié :
\begin{minted}{html}
{{^authenticated}}
  <p> contenu affiché lorsque authenticated est non défini ou contient une valeur fausse
{{/authenticated}}
\end{minted}

\subsection{Authentification}

\begin{enumerate}
  \item Ajoutez une fonction \verb+login(name, password)+ au modèle qui renvoie le numéro d'utilisateur s'il est bien authentifié, ou -1 sinon.
  \item Créez une vue \verb+views/login.html+ avec un formulaire d'authentification avec deux champs \verb+name+ et \verb+password+ et un bouton de soumission. L'action du formulaire sera d'appeler la route POST \verb+/login+.
  \item Ajoutez une route GET \verb+/login+ qui renvoie le formulaire d'authentification
  \item Ajoutez une route POST \verb+/login+ qui vérifie les identifiants passés dans \verb+req.body.name+ et \verb+req.body.password+ (nom des champs dans le formulaire), et ajoute l'identifiant et le nom de l'utilisateur à la session s'ils sont valides. En cas de succès, rediriger vers \verb+/+, sinon vers \verb+/login+.
  \item Ajouter une route POST \verb+/logout+ qui désauthentifie l'utilisateur (c'est à dire met la session à \verb+null+) et redirige le client vers \verb+/+. 
  %\item Ajouter un bout ``Se connecter" à la barre de menu lorsque l'utilisateur n'est pas connecté.
  \item Ajoutez le nom de l'utilisateur et un bouton ``Se déconnecter" dans la barre en haut de la page lorsque l'utilisateur est authentifié.
\end{enumerate}

\subsection{Création d'utilisateurs}

\begin{enumerate}
  \item Ajoutez une fonction \verb+new_user(name, password)+ au modèle qui créé un nouvel utilisateur dans la base et renvoie son identifiant.
  \item Créez une vue \verb+views/new_user.html+ avec un formulaire d'ajout d'utilisateur avec deux champs \verb+name+ et \verb+password+ et un bouton de soumission. L'action du formulaire sera d'appeler la route POST \verb+/new_user+.
  \item Ajoutez une route GET \verb+/new_user+ qui renvoie le formulaire d'ajout d'utilisateur
  \item Ajoutez une route POST \verb+/new_user+ qui ajoute effectivement l'utilisateur, l'enregistre dans la sessions et redirige le client vers \verb+/+
  \item Ajoutez deux boutons ``Se connecter" et ``Nouvel utilisateur" à la vue \verb+views/index.html+ si l'utilisateur n'est pas connecté.
\end{enumerate}

\section{Pour aller plus loin (optionnel)}

\begin{enumerate}
\item Cryptez le mot de passe de l'utilisateur avec \verb+bcrypt+ avant de le sauvegarder dans la base de données
\item Affichez des messages en cas de mauvais identifiants, ou pour avertir l'utilisateur qu'il s'est bien connecté ou déconnecté
\item Rendre le site conforme avec la RGPD
\end{enumerate}

\end{document}

% vi:set spell spelllang=fr:
