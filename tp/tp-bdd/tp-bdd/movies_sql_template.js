const fs = require('fs');
const Sqlite = require('better-sqlite3');

let db = new Sqlite('db.sqlite');

exports.load = function(filename) {
  const movies = JSON.parse(fs.readFileSync(filename));
  var insert = db.prepare('INSERT INTO movies VALUES (@id, @title, @year, @actors, @plot, @poster)');
  var insert_many = db.transaction((movies) => {
    db.prepare('DELETE FROM movies');
    for(var id of Object.keys(movies)) {
      insert.run(movies[id]);
    }
  });
  insert_many(movies);
  return true;
};

exports.save = function(filename) {
  let movie_list = db.prepare('SELECT * FROM movies ORDER BY id').all();
  let movies = {};
  for(movie of movie_list) {
    movies[movie.id] = movie;
  }
  fs.writeFileSync(filename, JSON.stringify(movies));
};

exports.list = function() {
  // TODO
};

exports.create = function(title, year, actors, plot, poster) {
  // TODO
};

exports.read = function(id) {
  // TODO
};

exports.update = function(id, title, year, actors, plot, poster) {
  // TODO
};

exports.delete = function(id) {
  // TODO
};

