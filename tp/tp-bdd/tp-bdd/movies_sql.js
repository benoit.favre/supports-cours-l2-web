const fs = require('fs');
const Sqlite = require('better-sqlite3');

let db = new Sqlite('db.sqlite');

db.prepare('DROP TABLE IF EXISTS movies').run();
db.prepare('CREATE TABLE movies (id INTEGER PRIMARY KEY AUTOINCREMENT, title TEXT, year INT, actors TEXT, plot TEXT, poster TEXT)').run();

exports.load = function(filename) {
  const movies = JSON.parse(fs.readFileSync(filename));
  var insert = db.prepare('INSERT INTO movies VALUES (@id, @title, @year, @actors, @plot, @poster)');
  var insert_many = db.transaction((movies) => {
    db.prepare('DELETE FROM movies').run();
    for(var id of Object.keys(movies)) {
      insert.run(movies[id]);
    }
  });
  insert_many(movies);
  return true;
};

exports.save = function(filename) {
  let movie_list = db.prepare('SELECT * FROM movies ORDER BY id').all();
  let movies = {};
  for(movie of movie_list) {
    movies[movie.id] = movie;
  }
  fs.writeFileSync(filename, JSON.stringify(movies));
};

exports.list = function() {
  let movie_list = db.prepare('SELECT * FROM movies ORDER BY id').all();
  return movie_list;
};

exports.create = function(title, year, actors, plot, poster) {
  let movie = {
    title: title,
    year: year,
    actors: actors,
    plot: plot,
    poster: poster,
  };
  let result = db.prepare('INSERT INTO movies (title, year, actors, plot, poster) VALUES (@title, @year, @actors, @plot, @poster)').run(movie);
  return result.lastInsertRowid;
};

exports.read = function(id) {
  result = db.prepare('SELECT * FROM movies WHERE id = ?').get(id);
  if(result === undefined) return null;
  return result;
};

exports.update = function(id, title, year, actors, plot, poster) {
  let movie = {
    id: id,
    title: title,
    year: year,
    actors: actors,
    plot: plot,
    poster: poster,
  };
  let result = db.prepare('UPDATE movies SET title = @title, year = @year, actors = @actors, plot = @plot, poster = @poster WHERE id = @id').run(movie);
  return result.changes == 1;
};

exports.delete = function(id) {
  let result = db.prepare('DELETE FROM movies WHERE id = ?').run(id);
  return result.changes == 1;
};

