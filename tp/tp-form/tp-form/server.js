"usr strict"

var express = require('express');
var mustache = require('mustache-express');
var fs = require('fs');

var app = express();
app.use((req, res, next) => { console.log(req.url); next(); });

app.engine('html', mustache());
app.set('view engine', 'html');
app.set('views', './views');

var movies = require('./movies');
movies.load('movies.json');

app.get('/', (req, res) => {
  res.render('movie-list', {movies: movies.list()});
});

app.get('/movie-details/:id', (req, res) => {
  res.render('movie-details', movies.read(req.params.id));
});

app.get('/edit-movie-form/:id', (req, res) => {
  res.render('edit-movie-form', movies.read(req.params.id));
});

app.get('/edit-movie/:id', (req, res) => {
  movies.update(req.params.id, req.query.title, req.query.year, req.query.actors, req.query.plot, req.query.poster,);
  res.redirect('/');
});

app.get('/add-movie-form', (req, res) => {
  res.render('add-movie-form');
});

app.get('/add-movie', (req, res) => {
  movies.create(req.query.title, req.query.year, req.query.actors, req.query.plot, req.query.poster);
  res.redirect('/');
});

app.get('/delete-movie-form/:id', (req, res) => {
  res.render('delete-movie-form', {id: req.params.id});
});

app.get('/delete-movie/:id', (req, res) => {
  movies.delete(req.params.id);
  res.redirect('/');
});

app.listen(3000, () => {
  console.log('Example app listening on port 3000!');
});
