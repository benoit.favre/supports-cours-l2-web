function init() {
  var form = document.createElement('form');
  var src = ['sounds/snare.wav', 'sounds/hho.wav', 'sounds/hhc.wav', 'sounds/bass.wav'];
  for(var i = 0; i < 4; i++) {
    for(var j = 0; j < 16; j++) {
      var input = document.createElement('input');
      input.type = 'checkbox';
      input.name='inst' + i + '_time' + j;
      input.classList.add('time' + j);
      input.classList.add('inst' + i);
      input.checked = false;
      form.appendChild(input);

      var audio = document.createElement('audio');
      audio.setAttribute('autobuffer', 'autobuffer');
      audio.src = src[i];
      form.appendChild(audio);
    }
    form.appendChild(document.createElement('br'));
  }
  document.body.appendChild(form);
  setInterval(update, 200);
}

var current = 0;

function update() {
  document.querySelectorAll('.time' + current).forEach(function(e) { e.classList.remove('playing'); });
  current = (current + 1) % 16;
  document.querySelectorAll('.time' + current).forEach(function(e) { e.classList.add('playing'); });
  var active = document.querySelectorAll('.time' + current + ':checked > audio');
  if(active) active.forEach(function(e) { e.play(); });
}

