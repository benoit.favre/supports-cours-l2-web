import urllib, json
import urllib.request
import urllib.parse
import sys

movies = {}

key = '5a7f42ff'
for line in open('titles.txt'):
    title = line.strip()
    try:
        url = 'http://www.omdbapi.com/?apikey=%s&t=%s&y=&plot=short&r=json' % (key, urllib.parse.quote(title))
        #print(url)
        data = json.loads(urllib.request.urlopen(url).read())
        if 'Title' in data:
            movie = {
                'id': len(movies),
                'title': data['Title'],
                'year': data['Year'],
                'actors': data['Actors'],
                'plot': data['Plot'],
                'poster': data['Poster'],
                }
            movies[movie['id']] = movie
        else:
            print(title, data, file=sys.stderr)
    except Exception as e:
        print(e, file=sys.stderr)

print(json.dumps(movies, indent=2, sort_keys=True))
