\documentclass{article}
\usepackage[margin=3cm]{geometry}
\usepackage[french]{babel}
\usepackage{graphicx}
\usepackage{url}
\usepackage{minted}

\title{Express et les formulaires}
\author{Benoit Favre}
\date{généré le \today}

\begin{document}
\maketitle

\section{Introduction}

On souhaite créer un site web permettant de gérer une collection de films. Le site permet de lister les films de la collection, d'ajouter un film, de voir les détails d'un film, de modifier les entrées correspondant à un film, et d'effacer un film.

\section{Gestion des données}

Dans la suite, un film sera modélisé comme un objet contenant les champs suivants :
\begin{itemize}
  \item \verb+id+ : l'identifiant du film, un nombre arbitraire
  \item \verb+title+ : le titre du film
  \item \verb+year+ : l'année de publication du film
  \item \verb+actors+ : la liste des acteurs séparés par des virgules
  \item \verb+plot+ : l'histoire du film
  \item \verb+poster+ : l'URL de l'affiche du film
\end{itemize}

Afin de séparer le modèle (la gestion des données) du reste du serveur, on vous fournit un module, \verb+movies+ qui permet de gérer les données en javascript.
Il offre les fonctionnalités suivantes :

\begin{itemize}
  \item \verb+movies.load(filename)+ : charger les films contenus dans un fichier JSON\footnote{Voir l'exemple {\tt movies.json} constitué à partir de \url{http://www.omdbapi.com/}}
  \item \verb+movies.save(filename)+ : sauvegarder les films dans un fichier json
  \item \verb+movies.list()+ : renvoie la liste des films, ordonnés par identifiant
  \item \verb+movies.create(title, year, actors, plot, poster)+ : créer un nouveau film avec les données passées en paramètres
  \item \verb+movies.read(id)+ : récupérer un objet contenant les champs du film identifié en paramètre, ou \verb+null+ si ce dernier n'existe pas
  \item \verb+movies.update(id, title, year, actors, plot, poster)+ : modifier les champs d'un film identifié par \verb+id+, renvoie \verb+false+ si l'identifiant n'existe pas
  \item \verb+movies.delete(id)+ : supprimer le film identifié par \verb+id+
\end{itemize}

Exemple d'utilisation :
\begin{minted}{js}
  let movies = require('./movies');
  movies.load('movies.json');
  console.log(movies.list());
\end{minted}

\section{Serveur express}

L'objectif est de créer un serveur nodejs avec express qui permette de manipuler la base de données de films.
Voilà un exemple de serveur qui génère une page pour l'url \verb+/movie-details/<id>+ ou \verb+<id>+ est l'identifiant d'un film.


\begin{minted}{js}
  var express = require('express');
  var mustache = require('mustache-express');

  var app = express();
  app.engine('html', mustache());
  app.set('view engine', 'html');
  app.set('views', './views');

  var movies = require('./movies');
  movies.load('movies.json');

  app.get('/movie-details/:id', (req, res) => {
    res.render('movie-details', movies.read(req.params.id));
  });

  app.listen(3000, () => console.log('movie server at http://localhost:3000');
\end{minted}

Ce serveur utilise \verb+mustache-express+ pour générer la page à partir d'un template et des données représentant un film (le résultat de \verb+movies.read+).
Ce template, stocké dans le fichier \verb+views/movie-details.html+, pourrait contenir le code HTML suivant :

\begin{minted}{html}
<html>
  <body>
    <h1> Détails d'un film </h1>
      Titre : {{title}}<br>
      Année : {{year}}<br>
      Nom des acteurs : {{actors}}<br>
      Résumé de l'histoire : {{plot}}<br>
      <img src="{{poster}}" width="300">
  </body>
</html>
\end{minted}

\begin{enumerate}
  \item Faîtes tourner cet exemple et vérifiez qu'il marche pour quelques films. Essayez par exemple avec \url{http://127.0.0.1:3000/movie-details/0}.
  \item Ajoutez une route pour \verb+/+ qui affiche la liste des titres des films précédés d'un lien \verb+détails+ qui permet de voir les détails du film. Vous devrez écrire un template appelé \verb+views/movie-list.html+ à qui vous passerez le résultat de \verb+movies.list()+.
\end{enumerate}

\section{Formulaires}

Les fonctionnalités de création, édition et suppression seront implémentées à l'aide de formulaires HTML. Chaque formulaire sera associé à une route GET qui effectuera l'action et redirigera l'utilisateur vers la page principale (\verb+/+). Par exemple, le principe pour l'ajout de films est le suivant :
\begin{enumerate}
  \item l'utilisateur clique sur le lien d'ajout d'un filme sur la page principale
  \item le serveur renvoie le formulaire permettant d'ajouter un film
  \item l'utilisateur remplit les différents champs et clique sur le bouton (input de type submit)
  \item le navigateur demande au serveur l'url spécifiée dans le champ \verb+action+ du formulaire
  \item le serveur effectue l'action demandée (ici, ajouter le film)
  \item le serveur redirige son client vers la page principale (avec \verb+res.redirect+ fournit par express)
\end{enumerate}

Les routes suivantes permettront d'afficher les formulaires correspondant aux fonctionnalités :
\begin{itemize}
  \item \verb'/add-movie-form' : affiche le formulaire\footnote{On utilisera ici la méthode GET, ce qui passe les valeurs des champs dans l'URL, qui peuvent être récupérés côté serveur dans {\tt req.query.nom\_du\_champ}.} de création d'un nouveau film (template \verb+views/add-movie-form+)
  \item \verb'/edit-movie-form/:id' : affiche le formulaire de modification d'un film existant (template \verb+views/edit-movie-form+)
  \item \verb'/delete-movie-form/:id' : affiche un formulaire de confirmation de la suppression d'un film (template \verb+views/delete-movie-form+)
\end{itemize}

À chaque formulaire est associé une route qui effectue l'action :
\begin{itemize}
  \item \verb'/add-movie' : ajouter le film passé en paramètre à la requête et rediriger le client vers /
  \item \verb'/edit-movie/:id' : modifier le film passé en paramètre à la requête et rediriger le client vers /
  \item \verb'/delete-movie/:id' : supprimer le film et rediriger le client vers /
\end{itemize}

Par exemple, pour la suppression, le formulaire \verb+views/delete-movie-form+ est le suivant :
\begin{minted}{html}
<!DOCTYPE html>
<html>
  <body>
    Voulez-vous vraiment supprimer "{{title}}" ?
    <form action="/delete-movie/{{id}}" method="get">
      <input type="submit" value="Supprimer">
    </form>
  </body>
</html>
\end{minted}

L'utilisateur peut soit retourner en arrière soit cliquer sur supprimer, ce qui appelle la route suivante :
\begin{minted}{js}
app.get('/delete-movie/:id', (req, res) => {
  movies.delete(req.params.id);
  res.redirect('/');
});
\end{minted}

\begin{enumerate}
  \item Implémentez ces différentes routes afin de rendre votre site opérationnel
  \item Sauvegardez la base de données après chaque modification
\end{enumerate}

\section{Pour aller plus loin (optionnel)}

Il n'y a aucune validation des données passées par l'utilisateur au serveur lors de la création / modification des films.
\begin{enumerate}
  \item Utilisez des routes POST à la place de GET pour effectuer les actions liées aux formulaires (valeurs des champs disponibles dans \verb+req.body+).
  \item Utilisez le module \verb+express-validator+ pour vous assurer que ces données sont valides (par exemple l'année est bien un nombre). N'oubliez pas d'utiliser \verb+body-parser+ ou autre pour lui donner accès aux variables de formulaire.
\end{enumerate}


\end{document}

% vi:set spell spelllang=fr:
