import sys
import json
import urllib.parse
import urllib.request
import re

def get_images(obj):
    if type(obj) == list:
        for item in obj:
            get_images(item)
    elif type(obj) == dict:
        for k, v in obj.items():
            get_images(v)
    elif type(obj) == str:
        if obj.startswith('http://') or obj.startswith('https:'):
            if obj.startswith('https://cuisine-facile.com/miniature.php'):
                obj = 'https://cuisine-facile.com' + obj.split('lien=')[1].split('&')[0]
            print(obj)

get_images(json.loads(open(sys.argv[1]).read()))
