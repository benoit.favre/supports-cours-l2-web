import json
import sys

if len(sys.argv) != 3:
    print('usage: pyhton %s <input.json> <output.json>' % sys.argv[0])
    quit(1)

with open(sys.argv[1]) as fp:
    data = json.loads(fp.read())

for entry in data:
    entry['img'] = entry['img'].replace('https://cuisine-facile.com/', 'https://pageperso.lis-lab.fr/benoit.favre/tp-web/')
    for ingredient in entry['ingredients']:
        del ingredient['img']
    entry['duration'] = entry['time']['Totale']
    del entry['time']
    for stage in entry['stages']:
        del stage['img']
        del stage['duration']
        del stage['title']

with open(sys.argv[2], 'w') as fp:
    fp.write(json.dumps(data, indent=2))
