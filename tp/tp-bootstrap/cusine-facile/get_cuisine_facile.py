from bs4 import BeautifulSoup
import re, json

import urllib.parse
import urllib.request

def get_main():
    urls = []
    url = 'https://cuisine-facile.com/liste.php'
    html = urllib.request.urlopen(url).read()
    soup = BeautifulSoup(html, 'html.parser')
    for found in soup.findAll("h5"):
        a = found.find('a')
        if not('/trucs_astuces/' in a['href'] or '/legendes/' in a['href']):
            #print(a['href'])
            urls.append(a['href'])
    return urls

def get_recipe(uri):
    base = 'https://cuisine-facile.com'
    url = base + uri

    html = urllib.request.urlopen(url).read()
    soup = BeautifulSoup(html, 'html.parser')

    recipe = {}

    main = soup.find('div', {'class': 'jumbotron pt-1 pb-1 text-left mt-2 mb-3'})
    recipe['title'] = main.find('h1').get_text()
    recipe['img'] = base + main.find('img', {'class': 'rounded align-self-center mr-3 img-fluid'})['src']
    recipe['description'] = main.find('div', {'class': 'col align-self-center'}).get_text().strip()

    recipe['ingredients'] = []
    ingredients = soup.find('section', {'id': 'ingredients'})
    for ingredient in ingredients.findAll('li', {'class': "list-group-item pt-2 pb-2"}):
        ingredient.find('span').get_text()
        img = base + ingredient.find('img')['src']
        name = re.sub('^\d+ ', '', ingredient.get_text()).strip()
        recipe['ingredients'].append({'name': name, 'img': img})

    time = soup.find('section', {'id': 'time'})
    time_headers = time.findAll('th')
    time_values = time.findAll('td')
    recipe['time'] = {}
    for header, value in zip(time_headers, time_values):
        recipe['time'][header.get_text()] = value.get_text()

    recipe['stages'] = []
    stages = soup.find('section', {'id': 'etapes'})
    for stage in stages.findAll('div', {'class': 'row mt-3 mb-2'}):
        title = stage.find('h5').get_text()
        duration = None
        if '-' in title:
            title, duration = title.split(' - ')
            duration = duration.strip()
        img = None
        if 'img' in stage:
            img = base + stage.find('img', {'class': 'rounded img-fluid float-left mr-3'})['src']
        description = stage.find('div', {'class': 'col align-self-center'}).get_text()
        recipe['stages'].append({'title': title, 'img': img, 'description': description, 'duration': duration})

    return recipe

urls = get_main()
recipes = []
#with open('urls') as fp:
#for uri in fp:
for uri in urls:
    try:
        recipe = get_recipe(uri.strip())
        recipes.append(recipe)
    except:
        pass

print(json.dumps(recipes))
