cat images.txt | while read
do
  img=$REPLY
  dest=`echo "$img" | sed 's%https://cuisine-facile.com/%%'`
  dir=`dirname "$dest"`
  mkdir -p "$dir"
  #echo $img $dir $dest
  wget $img -O $dest
done
