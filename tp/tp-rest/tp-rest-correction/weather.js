"use strict"

// installer les dépendences : npm install request mustache
let http = require('http');
let url_parser = require('url');
let request = require('request') ;
let mustache = require('mustache');
let fs = require('fs');

// obtenir un token et utiliser le service : https://weatherstack.com/documentation
let key = '4fdcbdb9cf74ecee5c916bef23d6584b';

let template = fs.readFileSync('weather.html').toString();
console.log('template', template);

http.createServer(function(req, res) {
  let url = url_parser.parse(req.url, true);
  console.log('url', url);

  let city;
  if (url.path != '/') city = url.pathname.substr(1);
  else city = url.query.city;
  if (city == '') city = 'Marseille';

  let weather = `http://api.weatherstack.com/current?access_key=${key}&query=${city}`
  request(weather, function(err, _, body) {
    if(!err) {
      let data = JSON.parse(body);
      console.log('weather', data);
      let html = mustache.render(template, data);
      res.writeHead(200, {'content-type': 'text/html'});
      res.end(html);
    }
    res.end(body);
  })
}).listen(3000);

