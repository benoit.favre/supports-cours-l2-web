\documentclass{article}
\usepackage[margin=3cm]{geometry}
\usepackage[french]{babel}
\usepackage{graphicx}
\usepackage{url}
\usepackage{minted}
\sloppy

\title{Page de météo : TP sur 2 séances}
\author{Benoit Favre}
\date{généré le \today}

\begin{document}
\maketitle

\section{Introduction}

L'objectif de ce TP est de créer un serveur web qui aille chercher des informations sur un autre site en exploitant une API REST\footnote{Pour une URL donnée, le site renvoie des informations au format JSON}, les formate en utilisant des templates et renvoie la page web générée à l'utilisateur.

\begin{center}
  \includegraphics[width=.4\textwidth]{rest-weather.png}
\end{center}

Il est recommandé pour tous les scripts javascript, d'utiliser le mode strict (ajouter \verb+"use strict"+ seul sur une ligne en haut de tous les scripts). Cela force par exemple à déclarer toutes les variables.

\section{Premier serveur web}

Voici un court exemple de serveur web qui écoute sur le port 3000 sur localhost. Quelle que soit l'url demandée, il renvoie la même page HTML.

\begin{minted}{js}
let url_parser = require('url')
let http = require('http')

let server = http.createServer((req, res) => {
  let url = url_parser.parse(req.url, true);
  console.log(url);
  res.writeHead(200, {'content-type': 'text/html'});
  res.end('<h1> Température à Marseille </h1>');
  } 
});

server.listen(3000);
\end{minted}

Modifiez ce serveur pour qu'il renvoie une page fictive de météo reproduisant les informations montrées dans la copie d'écran en début de sujet\footnote{Nuages : \url{https://assets.weatherstack.com/images/wsymbols01_png_64/wsymbol_0018_cloudy_with_heavy_rain.png}}.

\section{l'API de weatherstack.com}

Le site weatherstack.com offre une API REST pour accéder à des informations sur le temps qu'il fait dans une ville dans le monde. Le compte gratuit permet d'accéder à ces données avec des limitations. Notez que ce TP peut être fait avec une autre API sans trop de problèmes.

\begin{enumerate}
  \item Créez un compte sur le site et récupérez la clé d'accès à l'API (\url{https://weatherstack.com/signup/free}). La clé est ensuite visible dans \url{https://weatherstack.com/dashboard} une fois connecté.
  \item Dans un terminal, utilisez \verb+curl+ pour faire quelques requêtes. Lisez la documentation pour voir ce que vous pouvez faire. Par exemple, l'url \url{http://api.weatherstack.com/current?access_key=API_KEY&query=Marseille}, dans laquelle vous avez préalablement remplacé \verb+API_KEY+ par votre clé, permet de récupérer la météo sur Marseille au format JSON\footnote{Le format JSON est un sous-ensemble de javascript. Il s'agit d'une combinaison de nombres, chaînes de caractères, booléens, tableaux et dictionnaires. Vous pouvez obtenir une version formatée du JSON avec {\tt curl -s URL|python -m json.tool}}.
\end{enumerate}

\section{Charger une URL depuis node.js}

En node.js, le module \verb+request+ permet d'effectuer une requête HTTP sur un serveur distant. On peut l'installer avec \verb+npm install request+ en ligne de commande dans le répertoire du serveur. Ceci créé un répertoire \verb+node_modules+ qui contient tous les modules installés de cette manière. Il faut noter que \verb+npm+ s'occupe tout seul d'installer les dépendances de \verb+request+, ces autres modules étant aussi rangés dans le même répertoire. Une fois installé, un module peut être utilisé dans un script \verb+node+ avec l'instruction \verb+let module = require('nom du module');+.

Il se trouve que \verb+request+ est un module simple. Il n'est constitué que de la fonction \verb+request(url, callback)+ à qui on passe l'url que l'on veut charger et une fonction qui sera exécutée une fois que les données ont été chargées, ou lorsqu'une erreur arrive. Comme expliqué dans la documentation\footnote{\url{https://www.npmjs.com/package/request}} du module, ce callback prend trois arguments : l'erreur s'il y en a eu une, un objet qui représente la réponse du serveur (avec par exemple les entêtes et le code de retour HTTP) et le corps du message renvoyé par le serveur. C'est ce dernier qui nous intéresse.

Voilà un exemple de script pour demander au site weatherstack le temps qu'il fait à Marseille. Le serveur renvoie dans le body un texte représentant les données au format JSON. On peut les convertir en objet javascript avec \verb+JSON.parse(texte)+.

\begin{minted}{js}
let request = require('request');
let key = 'API_KEY'; // entrez votre API_KEY ici 
let city = 'Marseille';
let url = `http://api.weatherstack.com/current?access_key=${key}&query=${city}`;

console.log(url)
request(url, (err, res, body) => {
  if(!err) {
    let data = JSON.parse(body);
    // le résultat est disponible dans data, le traitement peut continuer ici
    console.log(data);
  }
})
// attention, le résultat ne sera jamais disponible en dehors du callback
\end{minted}

\begin{enumerate}
  \item Après avoir testé ce script, modifiez-le pour pouvoir passer le nom de la ville en argument au programme, en tapant dans le terminal quelque chose comme \verb+node get-weather.js Marseille+. Indice : utilisez \verb+process.argv+.
  \item Modifiez le script pour qu'il affiche à partir des données renvoyées par weatherstack :
    \begin{itemize}
      \item Le nom de la ville
      \item La température
      \item La vitesse du vent
      \item La description du temps qu'il fait (\verb+weather_descriptions+)
    \end{itemize}
\end{enumerate}

\section{Appel de l'API depuis votre serveur}

Modifiez votre serveur pour qu'il renvoie chercher le temps qu'il fait dans une ville de votre choix, et génère une page HTML simple contenant le nom de la ville, la température, la vitesse du vent, la description textuelle du temps qu'il fait et l'image représentant le temps qu'il fait (\verb+weather_icons+).

\section{Rendu avec des templates}

Dans le monde des serveurs web, un template est une page HTML générique qui contient des marqueurs de variables. Lors de l'exécution, on peut remplacer ces marqueurs par des valeurs spécifiées dans un objet javascript avant d'envoyer la page au client.

Il existe de nombreux moteurs de templates. Ces derniers permettent de séparer le code de l'application et le HTML, en rendant certains parties de ce dernier paramétrables. 
Le principe est de mettre toutes les données que l'on veut inclure dans le HTML dans un objet javascript, puis dans la page HTML d'indiquer où chacun des champs de l'objet doivent être remplacés.

Le moteur de template le plus simple pourrait ressembler à cela :
\begin{minted}{js}
let template = '<p> Température : {{temperature}} degrés</p>';
let temperature = 25;
// remplacer une occurence de {{temperature}} par la valeur de la variable temperature
let html = template.replace('{{temperature}}', temperature); 
console.log(html);
\end{minted}

Certains moteurs de templates sont très puissants et permettent d'utiliser des conditions et des boucles dans le HTML. Pour ce TP, nous allons en utiliser un très simple qui s'appelle Mustache (\url{https://github.com/janl/mustache.js/}).

On peut installer Mustache avec \verb+npm install mustache+. Ensuite, un script qui l'utilise ressemblerait à :
\begin{minted}{js}
let mustache = require('mustache');
let data = {x: 10, y: 3};
let template = "Les coordonnées sont {{x}} et {{y}}.";
console.log(mustache.render(template, data));
\end{minted}

\begin{enumerate}
  \item Lisez la documentation de Mustache pour comprendre comment il fonctionne. Deux façon d'employer les templates vont nous intéresser : les valeurs (\verb+{{temperature}}+) et les tableaux (\verb+{{#jours}} {{*}} {{/jours}}+). L'idée est de passer l'objet json retourné par weatherstack au moteur de template pour qu'il génère la page à partir des valeurs s'y trouvant.
  \item Écrivez une page html pour présenter les données de weatherstack (par exemple le nom de la ville, le temps qu'il fait, la température et le vent). Pour l'instant, utilisez des exemples de valeurs.
  \item Remplacez les exemples de valeurs par des templates. Pour tester, écrivez un script nodejs qui charge le template depuis un fichier, instancie un objet javascript avec des données et appelle mustache pour générer et afficher le HTML correspondant.
  \item Intégrez ce code dans votre serveur.
\end{enumerate}

Notez que l'on peut charger un fichier dans une chaîne de caractères avec \verb+fs.readFileSync('nom du fichier').toString()+. Cette fonctionnalité utilise le module \verb+fs+ qui fait partie de la librairie standard de node.

\section{Sélection de la ville}

Maintenant que le serveur sait aller chercher les données sur un serveur tiers, en faire une page HTML à partir d'un template, et les renvoyer au client, il ne reste plus qu'à laisser le client choisir la ville dont il veut la météo. Deux méthodes seront mises en place.

\subsection{Dans l'URL}

Dans la première méthode, on va considérer que l'utilisateur doit écrire le nom de la ville dans l'URL qu'il demande au serveur. L'URL sera de la forme \verb+http://127.0.0.1:3000/Nom-de-la-ville+. Pour récupérer le nom de ville demandé par l'utilisateur, il suffit d'utiliser le champ \verb+pathname+ de l'objet url renvoyé par \verb+url_parser+ à partir de son analyse de \verb+req.url+. Ce champ sera de la forme \verb+/Nom-de-la-ville+, donc il faudra enlever le slash.

Implémentez cette manière de choisir la ville et testez la avec \verb+curl+ et dans le navigateur.

\subsection{Avec un formulaire}

Il faut se l'avouer, devoir entrer la ville dans la barre d'URL du navigateur n'est pas très intuitif pour un utilisateur. Vous allez donc ajouter un formulaire à la page HTML pour que l'utilisateur puisse saisir le nom de la ville dans un champ prévu à cet effet et cliquer sur un bouton pour valider sa sélection. Voici à quoi pourrait ressembler un tel formulaire :

\begin{minted}{html}
<form action="/" method="GET">
  <input type="text" name="city" placeholer="Ville">
  <input type="submit" value="Valider">
</form>
\end{minted}

Il faut noter les éléments suivants :
\begin{itemize}
  \item Le formulaire est une balise \verb+<form>+ contenant deux balises \verb+<input>+.
  \item La première est de type \verb+text+ et a pour nom \verb+city+. L'attribut \verb+placeholder+ permet juste d'afficher un texte dans le champ pour aider l'utilisateur. Ce champ est celui dans lequel on va saisir la ville.
  \item La seconde est de type \verb+submit+. Ce champ est le bouton sur lequel l'utilisateur doit cliquer pour soumettre le formulaire.
  \item Pour le navigateur, soumettre le formulaire veut dire appeler l'URL spécifiée dans le champ \verb+action+ de la balise \verb+<form>+ avec en paramètres la valeur des champs ayant un attribut \verb+name+, et afficher la page renvoyée par le serveur.
\end{itemize}

Par exemple, si on entre \verb+Marseille+ comme nom de ville puis on clique sur \verb+Valider+, le navigateur va charger l'URL : \verb+http://127.0.0.1:3000/?city=Marseille+.
Côté serveur nodejs, après traitement de l'URL par \verb+url_parser+, on reçoit la valeur du paramètre \verb+city+ dans \verb+url.query.city+.

\begin{enumerate}
  \item Modifiez le template de la page HTML pour y ajouter le formulaire.
  \item Prenez en compte la ville choisie dans le formulaire à travers \verb+url.query.city+.
\end{enumerate}

Bravo, vous avez conçu un site web simple qui va chercher des informations sur un serveur REST en fonction de ce que veut l'utilisateur et présente le résultat sous forme lisible par un humain. Ouf.

\section{Pour aller plus loin (optionnel)}

\begin{enumerate}
  \item Redemander à weatherstack le temps qu'il fait à chaque requête n'est pas très économe (surtout qu'un compte gratuit est limité à 1000 requêtes par jour). Ajoutez un cache à votre serveur. Vous pouvez utiliser un dictionnaire qui contient pour une ville donnée la réponse de weatherstack et la date à laquelle la requête a été effectuée (utiliser \verb+new Date()+). Lors d'une nouvelle requête pour cette ville, si la date est moins récente que 10 minutes, retournez la valeur du cache, sinon mettez à jour cette valeur avant de la retourner.
  \item Choisissiez une API sur \url{https://apilist.fun/} et développez un serveur web pour donner accès aux données proposées par cette API.
\end{enumerate}

\end{document}

% vi:set spell spelllang=fr:
