let express = require('express');
let app = express();

app.use(express.urlencoded({
  extended: false
}));

function handler(req, res) {
  let state = 0;
  if (req.method == 'POST' && 'state' in req.body) state = parseInt(req.body.state);
  next_state = state + 1;
  res.send(`<h1>${state}</h1><form action="/counter" method="post">
    <input type="hidden" name="state" value="${next_state}">
    <input type="submit" value="add one">
    </form>`);
}

app.post('/counter', handler);
app.get('/counter', handler); // first time

app.listen(3000);

