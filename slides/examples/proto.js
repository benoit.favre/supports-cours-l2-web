let app = require('express')();

app.get('/', (req, res) => {
  res.send('<a href="/form">Formulaire</a>');
});

app.get('/form', (req, res) => {
  res.send(`
    <form action="/save" method="get">
      <input type="text" name="data">
      <input type="submit">
    </form>
  `);
});

app.get('/save', (req, res) => {
  console.log(req.query.data);
  res.redirect('/');
});

app.listen(3000);
