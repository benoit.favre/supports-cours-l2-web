var http = require('http'), fs = require('fs'), mime = require('mime');
http.createServer(function (req, res) {
    console.log(req.url);
    var filename = 'docroot/' + req.url;
    if(req.url == '/')
      filename = 'docroot/index.html';
    if(fs.existsSync(filename)) { // renvoie un fichier
      res.writeHead(200, {'Content-Type': mime.lookup(filename)});
      res.end(fs.readFileSync(filename));
    } else { // le fichier n'a pas été trouvé
      res.writeHead(404, {'Content-Type': 'text/plain'});
      res.end('page not found');
    }
}).listen(1337, '127.0.0.1');
console.log('Server running at http://127.0.0.1:1337/');
