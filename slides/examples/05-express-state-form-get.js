let express = require('express');
let app = express();

app.get('/counter', (req, res) => {
  let state = 0;
  if ('state' in req.query) state = parseInt(req.query.state);
  next_state = state + 1;
  res.send(`<h1>${state}</h1><form action="/counter" method="get">
    <input type="hidden" name="state" value="${next_state}">
    <input type="submit" value="add one">
    </form>`);
});

app.listen(3000);

