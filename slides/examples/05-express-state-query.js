let express = require('express');
let app = express();

app.get('/counter', (req, res) => {
  let state = 0;
  if ('state' in req.query) state = parseInt(req.query.state);
  let next_state = state + 1;
  res.send(`<h1>${state}</h1><a href="/counter?state=${next_state}">add one</a>`);
});

app.listen(3000);
