var http = require('http');
http.createServer(function (req, res) {
    if(req.url == '/') {
        res.writeHead(200, {'Content-Type': 'text/html'});
        res.end('<html>\n'
              + '    <head>\n'
              + '        <script src="say-hello.js"></script>\n'
              + '    </head>\n'
              + '</html>\n');
    } else if(req.url == '/say-hello.js') {
        res.writeHead(200, {'Content-Type': 'text/javascript'});
        res.end('alert("Hello dude!");');
    } else {
        res.writeHead(404, {'Content-Type': 'text/plain'});
        res.end('page not found');
    }
}).listen(1337, '127.0.0.1');
console.log('Server running at http://127.0.0.1:1337/');

