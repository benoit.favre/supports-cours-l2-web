
let express = require('express');
let cookieParser = require('cookie-parser');
let app = express();
app.use(cookieParser());

app.get('/counter', (req, res) => {
  let state = 0;
  if ('state' in req.cookies) state = parseInt(req.cookies.state);
  let next_state = state + 1;
  res.cookie('state', next_state);
  res.send(`<h1>${state}</h1><a href="/counter">add one</a>`);
});

app.listen(3000);
