let express = require('express');
let app = express();

app.get('/counter/:state', (req, res) => {
  let state = parseInt(req.params.state);
  let next_state = state + 1;
  res.send(`<h1>${state}</h1><a href="/counter/${next_state}">add one</a>`);
});

app.get('/counter', (req, res) => {
  res.send(`<h1>0</h1><a href="/counter/1">add one</a>`);
});

app.listen(3000);
