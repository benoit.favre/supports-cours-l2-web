Lang: french
Date: 2020-2021
Title: [Web] Web : node-js \\ Licence Informatique L2
Author: [B. Favre] Benoit Favre
Institute: [AMU] Aix Marseille Université
Theme: Boadilla
Coloring: javascript


=== Échanges client-serveur ===

--- Définitions préalables ---

* Internet : Ensemble de protocoles et technologies interconnectées
%* Le web : Le bout d'Internet qui contient des pages web.
* Addresse IP : identifiant numérique d'un élément connecté au réseau 
  * exemples IPv4 : \verb+192.168.1.1+, {\color{darkgreen} \verb+127.0.0.1+} (boucle locale), {\color{blue} \verb+0.0.0.0+} (toutes les interfaces)
  * exemples IPv6 : \verb+2a01:cb1c:e78:1b00:7816:ee00:a5e9:9b82+, {\color{darkgreen} \verb+::1+}, {\color{blue}\verb+::+}
* DNS (Domain Name Server) : serveur qui donne l'adresse IP associée à un nom de domaine
%* TCP (Transport Control Protocol) : protocole qui gère la retransmission des données perdues
* Connexion : lien d'échange entre deux programmes caractérisé par deux adresses IP et deux numéros de port
%* Socket : connexion du point de vue d'un programme, maintenue par le système d'exploitation
* Serveur : programme qui attend des connexions sur un port et offre un service à ses clients (ex: fournir des pages web)
* Client : programme qui se connecte à un serveur, lui demande un service et utilise le résultat (ex: un navigateur web)
* URL (Universal Resource Locator) : adresse permettant d'accéder à une ressource d'un serveur web.
%* RFC (Request For Comment) : norme du web
%* HTML (Hyper Text Markup Language) : langage pour décrire le rendu d'une page web
* HTTP/HTTPS (Hyper Text Transport Protocol) : protocole d'échange entre un serveur web et un navigateur

--- Le web, comment ça marche ? ---

[100#client-server.pdf]

--- Communication HTTP (non cryptée) ---

* L'utilisateur entre une URL dans le navigateur
* Le navigateur 
    # fait une requête DNS pour obtenir l'adresse du serveur
    # se connecte à l'addresse IP sur le port 80
    # envoie une commande HTTP et le chemin de la ressource
    # puis éventuellement des entêtes et un message
* Le serveur
    # va chercher ou génère la ressource demandée (par exemple un fichier html)
    # envoie un code de réponse
    # puis des entêtes
    # et enfin le contenu de la ressource (la page html)
* Le navigateur reçoit le résultat
* Et fait le rendu de la page web

--- HTTPS : HTTP crypté ---

* Pourquoi crypter ?
  * Éviter que quelqu'un regarde ce qui transite sur le réseau 
    * Mais on peut quand même savoir à quel serveur vous vous connectez
  * Éviter que quelqu'un de malveillant se fasse passer pour le serveur (attaque "man in the middle")
* HTTPS = HTTP + TLS/SSL (Transport Layer Security, Secure Socket Layer)
  * Utilise le port 443 au lieu de 80
  * Le déroulement est le même que pour HTTP sauf la connexion pendant laquelle la sécurité est établie
  * Repose sur la cryptographie à paire de clé publique/privée
* Authentification du serveur
  * L'adresse IP à laquelle on parle correspond-elle bien au serveur qu'on cherche à contacter ?
  * Certificats de sécurité : déléguer l'autorité à un tiers de confiance par un système de signatures 
* Cryptage de la communication
  * Échange de clés de cryptage aléatoires (par exemple avec l'algorithme Diffie-Hellman)
  * Puis cryptage de tous les octets de la communication

--- Le système d'authentification de TLS/SSL ---

* Objectif
  * S'assurer qu'on parle bien à "gnu.org" quand on se connecte à "gnu.org"
  * Mettre en place une communication cryptée (pas vu en détail ici)

* Cryptographie asymétrique 
  * Paire de clé {\color{red} privée} (secrète) et {\color{darkgreen} publique} (distribuable)
  * Un message crypté avec la clé {\color{red} privée} ne peut être décrypté qu'avec la clé {\color{darkgreen} publique} et inversement

* Application : signer un message M
  * A génère une version hachée H(M) et la crypte avec sa clé {\color{red} privée}. C'est la signature du message
  * A envoie à B le message et la signature
  * B décrypte la signature avec la clé {\color{darkgreen} publique} de A. Si c'est H(M), alors la signature est authentique

* Délégation de signature
  * Permet d'établir qu'un nom de domaine correspond à un serveur sans avoir sa clé publique préalablement
  * Utilise des clés publiques distribuées avec le système d'exploitation

--- Délégation de signature (avec Certification Authority/CA) ---

\begin{columns}\column{.35\textwidth}

{\footnotesize

# la clé publique du CA est distribuée dans le système d'exploitation
# le serveur signe ses informations et envoie ce certificat au CA
# le CA signe le certificat du serveur et le lui renvoie
# lorsqu'un client se connecte
# le serveur lui envoie le certificat doublement signé et sa clé publique
# le client peut établir l'authenticité du certificat grâce à la clé publique du CA, puis celle du serveur grâce à sa clé publique

}

\column{.65\textwidth}

[130#certificate-authority.pdf]

\end{columns}



%--- Aspects crypto ---

%* RSA

--- Cryptographie asymétrique : RSA ---

* Algorithme de cryptage à clé privée / clé publique
  * RSA du nom de ses auteurs Ronald Rivest, Adi Shamir, Leonard Adelman

* Création de la paire de clés
  # choisir aléatoirement $p$ et $q$ deux nombres premiers distincts
  # calculer $n=p\cdot q$, le module de chiffrement
  # calculer $\phi = (p - 1)(q - 1)$
  # choisir aléatoirement $e<\phi$ tel que $pgcd(e, \phi) = 1)$
  # calculer $d$ tel que de $d\cdot e \mod \phi = 1$ (inverse modulo $\phi$)

* $(e, n)$ est la clé {\color{darkgreen} publique}, $(d, n)$ est la clé {\color{red} privée}
  * Crypter un message $m$ avec la clé privée : $c = m^d \mod n$
  * Décrypter $c$ avec la clé publique : $m = c^e \mod n$

* La sécurité est assurée par le fait qu'il faut factoriser $n=p\cdot q$ pour retrouver $d$
  ce qui est difficile lorsque $p$ et $q$ sont grands.

--- RSA : exemple ---

* Création de la paire de clés
  # on choisit aléatoirement $p = 3$ et $q = 11$ premiers
  # on calcule $n = 3 \times 11 = 33$
  # on calcule $\phi = (3 - 1) \times (11 - 1) = 2 \times 10 = 20$
  # on choisit aléatoirement $e = 3$ (premier avec 20), la clé {\color{darkgreen} publique} est donc $(3, 33)$
  # $d = 7$, car $e\cdot d \mod \phi = 3 \times 7 \mod 20 = 1$, la clé {\color{red} privée} est donc $(7, 33)$

* Crypter le message $m=25$ (il faut $m<n$)
  * $c = m^d \mod n = 25^7 \mod 33 = 31$
* Décrypter $c$
  * $m = c^e \mod n = 31^3 \mod 33 = 25$

* À noter : on peut aussi crypter avec la clé publique et décrypter avec la clé privée


--- Algorithme de Diffie-Hellman pour l'échange de clé ---

\begin{columns}\column{.6\textwidth}

* Problématique
  * La cryptographie asymétrique est trop coûteuse pour chiffrer toute la communication
  * Diffie-Hellman permet d'échanger une clé de cryptage symétrique sans qu'un tiers n'en prenne connaissance

* L'algorithme
  # A et B s'échangent en clair un nombre premier $p$ et un nombre quelconque $g<p$
  # A génère aléatoirement $a$ et envoie $x=g^a \mod p$ à B
  # B génère aléatoirement $b$ et envoie $y=g^b \mod p$ à A
  # Tous deux calculent le secret partagé $c = x^b \mod p = g^{ab} \mod p = g^{ba} \mod p = y^a \mod p$
  # $c$ est utilisé comme clé de cryptage symétrique pour la communication

* Il est difficile de retrouver $c$ à partir de $g^a \mod p$ et $g^b \mod p$

%* Les version actuelles utilisent des courbes elliptiques au lieu du modulo mais le principe reste le même

\column{.4\textwidth}

[100#diffie-hellman.png]

\end{columns}
  
--- Communication HTTP : exemple ---

%%% terminal
> telnet www.google.com 80
Trying 173.194.67.104...
Connected to www.google.com.
Escape character is '^]'.
GET / HTTP/1.1

HTTP/1.1 302 Found
Location: http://www.google.fr/
Cache-Control: private
Content-Type: text/html; charset=UTF-8
Set-Cookie: PREF=ID=6f0d743f90944ab9:FF=0:TM=1363959194:LM=1363959194:S=Q0qT5NbYuS8K1pd6; expires=Sun, 22-Mar-2015 13:33:14 GMT; path=/; domain=.google.com
Set-Cookie: NID=67=rIhqH9SjFDpIXpAgXsAEjrYR-Qh3pTMtLq21zYg8xIqLi_OAgVifOd-d1x-sn01wemOXsB5XEoOGLoJ16CGtz2NhPrsM85OvAU9NSN_uSoDtgk3h35RgYemH8gkwrUoN; expires=Sat, 21-Sep-2013 13:33:14 GMT; path=/; domain=.google.com; HttpOnly
Server: gws
Content-Length: 218
X-XSS-Protection: 1; mode=block

<HTML><HEAD><meta http-equiv="content-type" content="text/html;charset=utf-8">
<TITLE>302 Moved</TITLE></HEAD><BODY>
<H1>302 Moved</H1>
The document has moved
<A HREF="http://www.google.fr/">here</A>.
</BODY></HTML>
Connection closed by foreign host.
%%%

--- Universal Resource Locator (URL) ---

* Le contenu de la barre de navigation
* Permet d'identifier un objet (une page, une image, une vidéo...) auprès d'un serveur web
* Constitution : `http://www.google.com:80/search?query=luminy&language=fr`
    * `http://` : protocole de communication (peut être `https`, `ftp`, `ssh`...)
    * `www.google.fr` : nom de domaine du serveur (associé à une adresse IP par un DNS)
    * `80` : numéro de port (80 par défaut pour http, 443 pour https, 21 pour ftp...)
    * `/search` : nom de la page web
    * `query=luminy`, `language=fr` : paramètres de la page

D'autres exemples :
%* `http://www.univ-amu.fr/`
* `https://mail.google.com/mail/u/0/?shva=1#inbox/13d91f7`
* `http://ent.univ-amu.fr/render.userRootNode.uP;jsessionid=45D`
* `http://localhost:8080/login.php?user=foo`

--- Protocole HTTP ---

* Normes : RFC 2616 / \url{https://tools.ietf.org/html/rfc2616}
* Commande:
%%% terminal
<commande> <url> HTTP/<version> \n\r
%%%
* Réponse du serveur
%%% terminal
HTTP/<version> <code> <phrase explicative> \n\r
<entetes> \n\r
\n\r
<message> \n\r
%%%
* Entêtes
%%% terminal
<nom> : <valeur> \n\r
<nom> : <valeur> \n\r
%%%
* Message
%%% terminal
<contenu> \n\r
%%%

--- Commandes ---

* GET : récupérer une ressource sans la modifier
%%% terminal
GET /index.html HTTP/1.1
%%%
* HEAD : récupérer les entêtes sans la ressource
%%% terminal
HEAD /video.avi HTTP/1.1
%%%
* POST : envoyer des données au serveur (ex: champs d'un formulaire)
%%% terminal
POST /path/script.cgi HTTP/1.0
Content-Type: application/x-www-form-urlencoded
Content-Length: 32

home=Cosby&favorite+flavor=flies
%%%
* PUT : envoyer et remplacer une ressource du serveur
* DELETE : effacer une ressource du serveur

--- Codes de réponse ---

* Succès
    * 200 OK
    * 204 No content
* Redirection
    * 301 Moved permanently
    * 302 Moved temporarly
* Erreurs du client
    * 400 Bad request
    * 401 Unauthorized
    * 403 Forbidden
    * 404 Not found
* Erreurs du serveur
    * 500 Internal error
    * 501 Not implemented
    * 503 Service not available
    * 505 HTTP version not supported

--- Entêtes (client) ---

* Accept: text/plain
* Accept-Charset: utf-8
* Accept-Encoding: gzip, deflate
* Accept-Language: en-US
* Authorization: Basic QWxhZGRpbjpvcGVuIHNlc2FtZQ==
* User-agent: Mozilla/5.0 (X11; Linux x86\_64) AppleWebKit/537.22 (KHTML, like Gecko) Chrome/25.0.1364.172 Safari/537.22
* Connection: keep-alive
* Cookie: Version=1; Skin=new;
* Host: www.google:80
* Date: Tue, 26 Mar 2013 08:12:31 GMT
* If-Modified-Since: Mon, 25 Mar 2013 08:12:31 GMT
* Referer: http://en.wikipedia.org

--- Entêtes (serveur) ---

* Content-Encoding: gzip
* Content-Language: fr
* Content-Type: image/png
* Content-Disposition: attachment; filename="ma photo qui tue.png"
* Content-Length: 348
* Cache-Control: no-cache
* Pragma: no-cache
* Last-Modified: Tue, 15 Nov 2013 12:45:26 GMT
* Expires: Thu, 01 Dec 2014 16:00:00 GMT
* Location: http://www.w3.org/pub/WWW/People.html
* Server: Apache/2.4.1 (Unix)

--- Les principaux serveurs web ---

Serveurs de fichiers
* Apache
* Internet Information Services (IIS) de Microsoft
* nginx
* Tornado (facebook)
* GWS (google)

Langages web (coté serveur)
* PHP
* ASP
* Tomcat (java)
%* Perl
* Django (python)
* Ruby on rails
* go, rust...
* *nodejs*

=== Node.js et le HTTP ===

--- Node.js comme serveur HTTP ---

Node gère de base
* Connexion d'un nouveau client
* Lecture des entêtes
* Analyse de l'URL
* Envoi de la réponse
* Erreurs
* Sécurité (https)

Node nécessite des modules complémentaires pour gérer
* Un script par page comme PHP (state-less)
* L'état des clients
* L'envoi des fichiers avec la bonne déclaration de type
* Les uploads
* Les bases de données

--- Exemple de serveur web ---

%%% js
var http = require('http');
http.createServer(function (request, response) {
    response.writeHead(200, {'Content-Type': 'text/plain'});
    response.end('Hello World\n');
}).listen(1337, '127.0.0.1');
console.log('Server running at http://127.0.0.1:1337/');
%%%

%%% terminal
> node server.js
Server running at http://127.0.0.1:1337/
%%%

[50#hello-server.png]

--- Exemple de serveur web (détails) ---

%%% js
var http = require('http');
http.createServer(function (request, response) {
    response.writeHead(200, {'Content-Type': 'text/plain'});
    response.end('Hello World\n');
}).listen(1337, '127.0.0.1');
console.log('Server running at http://127.0.0.1:1337/');
%%%
* Charge le module http
* Crée un serveur web qui appelle une fonction callback à chaque requête
* Attend des connections en local (127.0.0.1) sur le port 1337 (le port 80/443 nécessite d'être admin)
* Affiche un message quand il est prêt
* Lors d'une connexion, le callback répond :
    * que tout s'est bien passé (code d'erreur 200)
    * spécifie l'entête de contenu comme étant du texte
    * envoie un message et termine la réponse

--- Requête et réponse ---

$\to$ voir \url{https://nodejs.org/api/http.html}

Requête
* `request.headers`: Dictionnaire des entêtes envoyées par le client
* `request.url`: url telle que donnée au serveur
* `request.method`: Commande http du client : GET, POST, etc

Réponse (type http.ServerResponse):
* `response.writeHead(code, headers)`
    * Envoie le code d'erreur (ou de succès, cf slides précédents)
    * Puis les entêtes passés sous forme de dictionnaire
%%% js
var page = "<html><body>coucou</body></html>";
headers = { 'Content-Length': page.length, 'Content-Type': 'text/html' };
%%%
    %* On peut aussi utiliser `response.setHeader(name, value)`
* `response.write(data, encoding)`
    * Ne pas appeler avant d'avoir envoyé les entêtes !
    * Envoie des données (selon un encodage optionnel, par défaut c'est utf8)
* `response.end()` et `response.end(data)`
    * Termine la réponse (et éventuellement envoie des données)

--- Serveur multi-pages ---

%%% js
var http = require('http');
http.createServer(function (req, res) {
    if(req.url == '/') {
        res.writeHead(200, {'Content-Type': 'text/html'});
        res.end('<html>\n'
              + '    <head>\n'
              + '        <script src="say-hello.js"></script>\n'
              + '    </head>\n'
              + '</html>\n');
    } else if(req.url == '/say-hello.js') {
        res.writeHead(200, {'Content-Type': 'text/javascript'});
        res.end('alert("Hello dude!");');
    } else {
        res.writeHead(404, {'Content-Type': 'text/plain'});
        res.end('page not found');
    }
}).listen(1337, '127.0.0.1');
console.log('Server running at http://127.0.0.1:1337/');
%%%

--- Paramètres de la page ---

Le module `url` permet d'analyser l'url donnée par le client et d'en extraire les paramètres.
%%% js
url_parser = require('url');
console.log(url_parser.parse('/status?name=bryan&location=kitchen', true));
%%%
%%% terminal
{ href: '/status?name=bryan&location=kitchen',
  search: '?name=ryan',
  query: { name: 'bryan', location: 'kitchen' },
  pathname: '/status' }
%%%
Notez que pour être conformes à la spécification d'une url, les paramètres et leurs valeurs ne doivent
pas contenir de caractères spéciaux (`=&?`) ni de lettres accentuées. 
* `escape()` protège le caractères spéciaux
* `unescape()` fait l'opération inverse
%%% js
escape('salut les amis!');
unescape('salut%20les%20amis%21');
%%%

--- Serveur de fichiers ---

* Objectif : site web à base de fichiers
    * Pas besoin de modifier le serveur pour changer une page
    * Les ressources (images, css, scripts) peuvent être dans des fichiers différents
    * On peut réutiliser les fichiers dans différentes pages
* Par convention, une url sans paramètres représente un nom de fichier depuis la racine du serveur (appelé documentroot).
    * Si ce fichier existe et qu'il est accessible en lecture
    * Lire le fichier et déterminer son type mime (`npm install mime` pour installer le module)
    * Le renvoyer au client
* *Attention* : il ne faut pas donner accès à tous les fichiers de l'ordinateur
%%% sh
/                   => documentroot/index.html
/image.png          => documentroot/image.png
/scripts/jquery.js  => documentroot/scripts/jquery.js
/../server.js       => documentroot/../serverjs ***
%%%

--- Serveur de fichiers ---

%%% js
var http = require('http'), fs = require('fs'), mime = require('mime');
http.createServer(function (req, res) {
    var filename = 'docroot/' + req.url;
    if(req.url == '/') 
      filename = 'docroot/index.html';
    if(fs.existsSync(filename)) { // renvoie un fichier
        res.writeHead(200, {'Content-Type': mime.lookup(filename)});
        res.end(fs.readFileSync(filename));
    } else { // le fichier n'a pas été trouvé
        res.writeHead(404, {'Content-Type': 'text/plain'});
        res.end('page not found');
    }
}).listen(1337, '127.0.0.1');
console.log('Server running at http://127.0.0.1:1337/');
%%%

# Transformer l'url en nom de fichier
# Vérifier si le fichier existe
# Lire et renvoyer le fichier ou une erreur, le cas échéant

--- Types MIME ---

* Multipurpose Internet Mail Extensions (MIME)
  * Définit le type d'un fichier en fonction de son extension
  * Entête "content-type"

%%% terminal
$ cat /etc/mime.types
...
text/plain                      txt asc text pm el c h cc hh cxx hxx f90 conf log
text/html                       html
text/css                        css
application/javascript          js
application/json                json
...
image/png                       png
image/jpeg                      jpg jpeg jpe jfif
...
video/mp4                       mp4 mpg4 m4v
video/x-msvideo                 avi
audio/mpeg                      mp3 mpga mp1 mp2
...
application/zip                 zip
%%%

--- Serveur HTTPS dans Node ---

* Nécessite une paire clé privée, certificat signé
  * `privkey.pem` : la clé privée
  * `cert.pem` : la chaîne de certificats signés

%%% js
var fs = require('fs');
var https = require('https');

var port = 443;
var options = {
  key: fs.readFileSync('privkey.pem'),
  cert: fs.readFileSync('cert.pem')
};

var server = https.createServer(options, function(req, res) {
  // ...
});
server.listen(443, function() { console.log(`https server on port ${port}!`); });
%%%

--- Générer un certificat TLS/SSL ---

* Autosigné (self-signed)
  * Il faut l'importer dans le navigateur pour qu'il soit reconnu

%%% terminal
# Générer le certificat
openssl req -x509 -newkey rsa:4096 -days 365 -subj '/CN=domaine.com' \
  -nodes -keyout privkey.pem -out cert.pem 
# Voir le contenu du certificat
openssl x509 -in cert.pem -text -noout
%%%

* Signé par une autorité de certification
  * Let's Encrypt (\url{https://letsencrypt.org/})
  * DigiCert
  * ...

* Souvent on utilise un serveur "proxy" comme nginx qui gère le HTTPS et
  fait suivre la communication décryptée au serveur web qui tourne en local.
  * Il vaut mieux déléguer la sécurité à ceux qui s'y connaissent

%=== Le web dynamique ===
%
%--- AJAX (Asynchronous JavaScript and XML) ---
%
%* javascript ne peut pas demander de nouvelles informations au serveur
%    * Ouvrir une nouvelle fenêtre : lourd et peu intuitif
%    * `<meta http-equiv="refresh">` : recommence une page de zéro
%    * `<iframe src="http://www.googlecom"></iframe>` : interactions limitées
%    * Flash : difficile a mettre en {\oe}uvre, ne supporte pas les standards
%
%* Que permet de faire AJAX ?
%    * Faire une requête sur un serveur HTTP
%    * Récupérer le résultat (en XML ou autre) et le traiter côté client
%    * Changer l'affichage en fonction du résultat
%
%* Limitations
%    * Politique du "même origine" : ne peut accéder qu'au même serveur
%    * Implémentation différente en fonction du navigateur
%
%* Utilisé par tous les sites web dynamiques (google, facebook...)
%
%--- L'objet XMLHttpRequest ---
%
%* XMLHttpRequest implémente un client http accessible en javascript depuis le navigateur
%* Différent dans les vielles versions d'Internet Explorer
%* Rien à changer côté serveur
%* La fonction suivante renvoie l'objet XMLHttpRequest
%%%% js
%function getXMLHttpRequestObject() {
%    var ref = null;
%    if (window.XMLHttpRequest) {
%        ref = new XMLHttpRequest();
%    } else if (window.ActiveXObject) { // internet explorer < 6
%        ref = new ActiveXObject("MSXML2.XMLHTTP.3.0");
%    }
%    return ref;
%}
%%%%
%
%--- Utilisation ---
%
%* Le champ `onreadystatechange` permet de spécifier une fonction callback qui traitera la réponse du serveur.
%* Le champ `readyState`, utilisé dans le callback, donne l'état de la réponse :
%    * 1 = ouverture réussie
%    * 2 = entêtes envoyées
%    * 3 = début de réception du contenu
%    * 4 = réponse reçue en intégralité
%* La méthode `open(command, url)` initie la connexion.
%    * La commande HTTP peut être `GET`, `POST`, `PUT`...
%    * L'url à aller chercher
%    * Ne fonctionne que sur le même serveur, même port (même origine)
%* La méthode `setRequestHeader(name, value)` permet de spécifier une entête qui sera reçue par le serveur.
%    * `name` : nom de l'en-tête (ex: `Accept-Language`)
%    * `value` : la valeur à associer (ex: `fr_FR`)
%* La méthode `send(data)` envoie le tout avec un corps de message optionnel (utilisé avec la méthode `POST`).
%
%--- Exemple ---
%
%%%% js
%var xhr = getXMLHttpRequestObject();
%xhr.onreadystatechange = function() {
%    console.log(xhr.readyState);
%    if (xhr.readyState == 4){
%        console.log(ref.status, ref.statusText); // 200 OK
%        alert(xhr.response); // texte du contenu chargé depuis le serveur
%    }
%};
%xhr.open('GET', '/temperature?annee=2013&mois=4', true);
%xhr.setRequestHeader('X-Requested-With', 'XMLHttpRequest'); // exemple d'entête courante
%xhr.send(null);
%%%%
%
%# Instancier le client HTTP
%# Spécifier un callback
%# Lorsque `readyState` est à 4, la réponse est prête
%# Ouvrir la connexion
%# Optionnel : spécifier une entête
%# Envoyer la requête (GET ne permet pas d'envoyer des données)
%
%--- Heureusement il y a jQuery ---
%
%Demander au serveur une information et mettre le résultat dans le DOM :
%%%% js
%var xhr = getXMLHttpRequestObject();
%xhr.onreadystatechange = function() {
%    if (xhr.readyState == 4){
%        var element = document.getElementById('temperature');
%        element.innerHTML = xhr.response;
%    }
%};
%xhr.open('GET', '/temperature?annee=2013&mois=4', true);
%xhr.send(null);
%%%%
%
%Même chose avec jQuery :
%%%% js
%$.get('/temperature?annee=2013&mois=4', function(data) {
%    $('#temperature').html(data);
%});
%%%%
%
%--- Exemple : client AJAX ---
%
%Page web lisant la date depuis un serveur (time-index.html)
%%%% html
%<html>
%    <head>
%        <script src="jquery.js"></script>
%        <script type='text/javascript'>
%            $(document).ready(function() { // attendre la fin du chargement
%                setInterval(function() {
%                    $.get('/time', function(data) { // appel ajax
%                        $('#time').html(data);
%                    });
%                }, 1000);
%            });
%        </script>
%    </head>
%    <body>
%        <div id="time"></div>
%    </body>
%</html>
%%%%

%--- Exemple : serveur ---
%
%Serveur correspondant (time-server.js)
%%%% js
%var http = require('http'), fs = require('fs'), mime = require('mime');
%http.createServer(function (req, res) {
%    var filename = '.' + req.url;
%    if(req.url == '/') filename = 'time-index.html';
%
%    if(req.url == '/time') { // renvoie la date courante sur le serveur
%        res.writeHead(200, {'Content-Type': 'text/plain'});
%        res.end(String(new Date()));
%
%    } else if(fs.existsSync(filename)) { // renvoie un fichier
%        res.writeHead(200, {'Content-Type': mime.lookup(filename)});
%        res.end(fs.readFileSync(filename));
%
%    } else { // le fichier n'a pas été trouvé
%        res.writeHead(404, {'Content-Type': 'text/plain'});
%        res.end('page not found');
%    }
%}).listen(1337, '127.0.0.1');
%console.log('Server running at http://127.0.0.1:1337/');
%%%%

%--- Autres exemples ---
%
%* Passer un paramètre
%%%% js
%parameters = 'annee=' + escape(annee) + '&' + 'mois=' + escape(mois);
%$.get('/temperature?' + parameters, ...);
%$.get('/temperature', {annee: annee, mois: mois}, ...);
%%%%
%* Récupérer un entier
%%%% js
%$.get('/get-age?name=jack', function(data) {
%    var age = parseInt(data);
%});
%%%%
%* Récupérer une liste
%%%% js
%res.writeHead(200, {'Content-Type': 'text/plain'});
%res.end(names.join(',')); // serveur
%-----
%$.get('/list-of-names', function(data) { // client
%    var names = data.split(',');
%    for(var i = 0; i < names.length; i++) console.log(names[i]);
%});
%%%%
%* Comment faire pour les structures plus compliquées ?

%--- jQuery et JSON ---
%
%* Envoyer du JSON côté serveur en spécifiant le `content-type`.
%%%% js
%var http = require('http');
%var dude = { name: 'John', talk: 'I love javascript'};
%var num = 0;
%http.createServer(function (request, response) {
%    if(request.url == '/say') {
%        num++;
%        response.writeHead(200, {'Content-Type': 'application/json'});
%        response.end(JSON.stringify({person: dude, num_calls: num}));
%    }
%    // il faut aussi traiter l'envoi de fichiers
%}).listen(1337, '127.0.0.1');
%%%%
%* jQuery sait qu'il faut désérialiser le JSON grâce au `content-type`.
%%%% js
%$.get('/say', function(data) {
%    console.log(data);
%    console.log(data.person.name + ' said "' + data.person.talk 
%        + '" ' + data.num_calls + ' times');
%});
%%%%
%
%--- Envoyer des objets depuis le client ---
%
%* Utiliser la commande HTTP "POST".
%* Côté client
%%%% js
%$.post('/add_meeting', JSON.stringify({ name: "John", time: "2pm" }) );
%%%%
%* Côté serveur c'est plus compliqué
%%%% js
%var http = require('http');
%var meetings = [];
%http.createServer(function (request, response) {
%    if(request.method == "POST" && request.url == '/add_meeting') {
%        var data = "";
%        request.on("data", function(chunk) {
%            data += chunk;
%        });
%        request.on("end", function() {
%            console.log(data);
%            meetings.push(JSON.parse(data)); // attention si JSON mal formé
%            // ...répondre
%        });
%    }
%    // ...traiter GET
%}).listen(1337, '127.0.0.1');
%%%%

%=== Conclusion ===
%
%--- Ce que vous avez vu ---
%
%* Javascript côté serveur
%    * Appels asynchrones
%    * Entrées-sorties
%    * Modules
%* Fonctionnement de HTTP
%    * Serveur de fichiers
%    * Serveur renvoyant des données à la demande
%* Le web dynamique
%    * AJAX
%    * JSON
%    * jQuery
%
%--- Pour devenir des pros ---
%
%* Côté client
%    * extensions jQuery
%    * 3d (WebGL)
%    * Faire un site web qui marche sur tous les navigateurs
%    * Optimiser une page
%* Côté serveur
%    * Upload de fichiers
%    * Frameworks web (express)
%    * Bases de données (mongodb)
%    * Communication push (websockets)
%    * Délestage de la charge

--- Conclusion ---

* Le web
  * Réseau d'ordinateurs
  * Client (navigateur) $\to$ Serveur (e.g. Node.js)
  * Ensemble de protocoles
    * HTTPS
    * Entêtes...

* Nodejs
  * Permet de faire des serveurs web
  * Mais aussi un langage d'applications normal

% vi:set spell spelllang=fr:
