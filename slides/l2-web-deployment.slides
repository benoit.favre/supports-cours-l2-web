Lang: french
Date: 2020-2021
Title: [Web] Web : node-js \\ Licence Informatique L2
Author: [B. Favre] Benoit Favre
Institute: [AMU] Aix Marseille Université
Theme: Boadilla
Coloring: javascript
Outline: Plan

=== Intro ===

--- Problématique générale ---

* L'hébergement Web est devenu compliqué mais intéressant
  * De plus en plus de maillons dans la chaîne
  * Sécurité et haute disponibilité
  * Un compromis entre dépenser de l'argent et faire sois-même à toutes les étapes

* Approche "Dev-Ops"
  * Ce cours vous donne les grandes lignes pour comprendre
    * Beaucoup de concepts seront vus en détails dans un cours de réseau
  * Mais pour tout faire vous-même mais il faudra certainement lire des tutoriels

* Étapes
  * Enregistrer un nom de domaine
  * Générer un certificat SSL auprès d'un CA
  * Configurer un reverse proxy
  * Choix d'hébergement
  * Faire vivre un site web
  * Passage à l'échelle


--- Rappel ---

[100#client-server.pdf]


=== Nom de domaine ===

--- Enregistrer un nom de domaine ---

* Le DNS (domain name service) permet d'associer un nom à une adresse IP
* Repose sur un système de délégation
  * Les domaines plus près de la racine gèrent ceux moins près de la racine
  * Qui est ametice.univ-amu.fr ?
    # ? .fr
    # ? .univ-amu.fr
    # ? ametice.univ-amu.fr

* On peut acheter (louer) un nom de domaine auprès d'un "registrar"
  * Géré par un DNS racine (`domaine`.fr, `domaine`.com)
  * En général pour une durée déterminée
  * Le coût dépend de la popularité 
  * Typiquement 3-15 euros par an
  * freenom, godaddy, namecheap, dreamhost, buydomains...

* DNS dynamique
  * Géré par un site (`domaine`.dyndns.fr, `domaine`.no-ip.com...)
  * Nécessite un renouvellement fréquent 


--- Mettre en place les entrées DNS ---

* Une fois le nom de domaine enregistré, il faut lui associer l'adresse du serveur
  * Typiquement un formulaire web

* Types d'enregistrements DNS\footnote{\url{https://fr.wikipedia.org/wiki/Domain_Name_System}}
  * A : une adresse IP v4
  * AAAA : une adresse IP v6
  * CNAME : la même adresse que le nom spécifié (un alias)
  * NS : serveur de nom du domaine (responsable des sous-domaines)

* Intervalle de mise à jour (time-to-live, TTL)
  * Temps pendant lequel une entrée est mise en cache

* Comment voir le résultat de la configuration
  * `# nslookup univ-amu.fr`
  * \url{https://dnsquery.org}, \url{https://mxtoolbox.com}


--- Certificat TLS/SSL ---

* Certifier l'association entre le nom du serveur et celui-ci
  * Voir cours sur TLS/SSL

* Autosigné (self-signed)
  * Il faut l'importer dans un navigateur pour qu'il soit reconnu
  * Pas une solution pour un site ouvert au public
  * Voir cours sur TLS/SSL

* Signé par une autorité de certification
  * Let's Encrypt (\url{https://letsencrypt.org/})
  * DigiCert
  * ...

* Problématiques
  * L'autorité de certification doit vérifier que vous êtes bien le propriétaire du serveur
  * La certification est temporaire, il faut renouveler les certificats régulièrement


--- Let's Encrypt ---

* Let's encrypt a pour objectif un web 100\% HTTPS
  * Rendre le processus accessible à tous
  * Automatiser un maximum d'étapes

* Protocole ACME (Automated Certificate Management Environment)
  * Nécessite que les entrées DNS soient déjà configurées
  * Permet de vérifier qu'on a un accès SSH sur le serveur
  * Génère un certificat signé par l'autorité de certification (CA)
  * Permet de renouveler un certificat
  * Permet de révoquer un certificat (par exemple si le serveur a été compromis)

* Certbot ACME client
  * Nécessite un serveur HTTP qui tourne (par exemple ngnix ou apache)
  * Tutoriel : \url{https://certbot.eff.org/instructions}
  * S'occupe de renouveler les certificats tous les 90 jours


--- Utilisation dans node-js ---

* Nécessite une paire clé privée, certificat signé
  * `privkey.pem` : la clé privée
  * `cert.pem` : la chaîne de certificats signés

%%% js
var fs = require('fs');
var https = require('https');

var port = 443;
var options = {
  key: fs.readFileSync('privkey.pem'),
  cert: fs.readFileSync('cert.pem')
};

var server = https.createServer(options, function(req, res) {
  // ...
});
server.listen(443, function() { console.log(`https server on port ${port}!`); });
%%%


--- Reverse proxy ---

\begin{columns}

\column{.5\textwidth}

* Motivation
  * Implémenter un serveur web sécurisé, sûr et rapide est difficile
  * Un site web a des parties statiques
  * Un serveur peut héberger plusieurs sites web (virtualhost)

\column{.5\textwidth}

[120#reverse-proxy.pdf]

\end{columns}

* Fonctionnement
  * Gère la sécurité des connexions HTTPS
  * Distribue les requêtes HTTP en fonction de règles
    * Début du chemin, nom d'hôte demandé, entêtes
  * Peut servir les fichiers statiques
  * Peut envoyer à un autre serveur en HTTP (par exemple nodejs)
  * Peut réécrire les requêtes (par exemple le chemin)

* Les plus connus : Nginx / Apache
  * Nginx : \url{https://www.sitepoint.com/configuring-nginx-ssl-node-js/}
  * Apache : \url{https://tecadmin.net/apache-frontend-proxy-nodejs/}


=== Hébergement ===

--- Hébergement sur un serveur ---

* Vraiment pas cher : raspberry PI
  * RPI 4 avec 8GB de RAM $<$ 100 euros
  * Suffisamment rapide pour un site perso, petit site collaboratif
  * Consomme peu d'électricité\footnote{Site web solaire : \url{https://solar.lowtechmagazine.com/about.html}} (environ 5W)

* Serveur dédié
  * Exemple : PowerEdge Dell 1U, 96GB mémoire, 12 coeurs, 1TB disque : 1500-3000 euros
  * Nécessite une armoire rack et une climatisation spécifique
  * Permet de faire tourner un site d'entreprise complexe de taille moyenne 

* Avantages
  * Contrôle total de ce qui se passe
  * Maîtrise des données et traitements

* Désavantages
  * En charge de la sécurité disponibilité et la sécurité
  * Connexion internet lente (typiquement une ligne ADSL/Fibre)


--- Location d'un serveur dans le cloud ---

* Dans un datacenter
  * Soit un serveur partagé (machine virtuelle), soit un serveur dédié "privatif"
  
* Acteurs principaux
  * Amazon Web Services
  * Google Cloud
  * Microsoft Azure
  * OVH...

* Avantages
  * Gestion complète, intégration à de nombreux services
    * SQL, Wordpress, Elasticsearch, backup...
  * Redimensionnement dynamique 
  * Réseau mondial divisé en régions qui correspondent aux continents
  * Permet d'économiser le coût des personnels dédiés 

* Désavantage
  * Coût récurrent, complexité 
  * Changer de fournisseur est fastidieux (vendor lock-in)


--- Coûts sur le cloud (2021) ---

* Location du serveur
  * le moins cher : 3 euros par mois
  * 1 coeur, 2GB RAM, 1GB stockage : environ 20 euros par mois
  * 4 coeurs, 8GB RAM, 100GB stockage : environ 100 euros par mois
  * 64 coeurs, 64GB RAM, 1TB stockage : $>$1000 euros par mois

* Transfert des données
  * En général appliqué aux données sortantes (envoyées du serveur au client)
  * Prix dégressif, de 0-10 TB/mois \$0.09 par GB
  * Site de 10 MB, 1 million de visiteurs par mois (10 TB) = \$900 par mois

* Stockage (backups)
  * Environ 20 centimes d'euro par mois par GB
  * Parfois, on ne paie que pour récupérer les données

=== Faire vivre un site web ===

--- Tester avant de déployer ---

* Objectif : faire des mises à jour sans interruption de service
  * Ne pas perdre d'information
  * Éviter les régressions 
  * Pouvoir revenir en arrière en cas de problème

* La base : ne pas développer sur une machine en production
  * Pendant le développement, utiliser un serveur jumeau (ex: "www-dev.entreprise.com")
  * Une fois les modifications validée, "pousser" vers le serveur en production (ex: "www.entreprise.com")
  * Ne jamais déployer avant de partir en vacances ;)

* Tests unitaires
  * Marche bien pour le "backend" (tout le code qui ne correspond pas à une interaction avec l'utilisateur)
  * Le principe des tests unitaires ne fonctionne pas très bien pour les interfaces utilisateurs
    * Tester abondamment sur le serveur de développement
    * Faire un déploiement partiel pour un petit pourcentage d'utilisateurs

* Contrôle de version et intégration continue
  * Chaque commit git donne lieu à un lancement de tous les tests non interactifs sur un serveur dédié
  * Si tous les tests passent, on peut directement appliquer la modification sur le serveur de développement


--- Déployer sans tout réinstaller ---

* Motivation
  * Installer un logiciel requiert souvent beaucoup de dépendances et de configuration
  * Administrer des serveurs est long et fastidieux (mises à jour, gérer la sécurité, backups...)
  * On aimerait avoir une image du système d'exploitation complet qu'il suffit de lancer

* Solution
  * Machines virtuelles légères
  * Permet de faire tourner plusieurs services sur la même machine sans qu'ils se voient
  * Permet de facilement migrer un service d'une machine vers une autre

* Exemple : conteneurs Docker (\url{https://docker.com})
  * Un format simple permet de décrire les étapes de création d'une image (DockerFile)
  * Une fois une image construite, elle peut être lancée dans un conteneur n'importe où
  * Un conteneur peut rendre disponible 
    * Des répertoire (données, configuration...)
    * Des ports (accès au service)
  * Un dépôt central facilite la récupération des images (\url{https://dockerhub.com})

* Standard dans l'industrie 
  * Kubernates (puissant mais complexe)


--- Sauvegardes ---

* La fameuse situation catastrophique
  * Panne d'un disque dûr (environ 1\%\footnote{\url{https://www.backblaze.com/blog/backblaze-hard-drive-stats-q2-2020/}} des disques par an)
  * Panne électrique / réseau
  * Vol des serveurs dans le datacenter
  * Cyptorançon, détournement du serveur par un pirate 
  * Incendie\footnote{\url{https://www.ovh.com/fr/news/presse/cpl1785.dernieres-informations-notre-site-strasbourg}}/inondation dans le datacenter
  * Décès ou départ d'un employé avec les mots de passe

* Comment organiser les backups
  * Crypter 
  * Intervalles réguliers
  * Sur des disques différents (local)
  * Dans des sites physiques différents 

* Attention toutefois aux empoisonnement de backups
  * Exemple, un pirate installe une backdoor qui se retrouve sauvegardée dans le backup
  * Garder des backup plus anciens (par exemple 1 an, 6 mois, 3 mois, 1 mois, 1 semaine)


--- Analytics (analyse d'utilisation) ---

* Que font les utilisateurs de mon site web ?
  * Ordre des pages visitées
  * D'où viennent-ils ?
  * Qui sont ils ?
  * Pourquoi un changement soudain du trafic ?

* Collecter des informations sur l'utilisateur
  * Le plus basique: exploiter les logs du serveur
    * Statistiques sur les url les plus visitées
  * Nécessite un suivi de la session pour reconstruire un parcours
  * Une partie du trafic provient de bots

* Services disponibles
  * Auto-géré (ex: \url{https://plausible.io})
  * Géré par d'autres
    * Google analytics \url{https://analytics.google.com/analytics/web/}
    * Allonge le temps de réponse et souvent supprimé par les anti-pub

* RGPD
  * Avertir l'utilisateur et maitriser les données
  * Ne pas tomber dans la vente à outrance des traces utilisateur


--- A/B testing ---

* Motivation
  * Nouvelle fonctionnalité: risque de perdre des utilisateurs
  * Répondre à des question sur le design (vaut-il mieux du rouge ou du bleu ?)
  * Comment décider de si ça vaut le coup ?

* Approche statistique du problème
  * Soit deux solutions A et B
  * Servir aléatoirement la solution A ou la solution B
  * Collecter des statistiques
    * Nombre de clics
    * Nombre de comptes créés
    * Nombre de ventes effectives
    * ...
  * Garder l'option qui marche le mieux
    * Faire des tests de significativité statistique (voir cours de statistiques)

* Multi-armed bandit
  * Méthode pour généraliser le test A/B à plus de deux options


--- Search Engine Optimisation (SEO) ---

* Moteur de recherche
  * Principal moyen d'acquisition de nouveaux utilisateurs
  * Utilisé de deux manières : découvrir des contenus ou naviguer vers un site connu

* Qu'est-ce qui fait qu'une page est bien placée dans les résultats
  * Mots clés utilisés dans la requête
  * Forme et contenu du site
  * Méta-données du site
  * Liens depuis d'autres site vers le site

* Google/Bing joue au chat et à la souris
  * L'algorithme de classement n'est pas public
    * Initialement basé sur PageRank, la popularité d'un site compté en liens entrant
  * Chaque fois qu'un site trouve une astuce pour biaiser son rang, Google modifie son algorithme pour le contrer
  * À travers le temps, on observe de grandes disparités de classement pour un même site

* On peut acheter des liens "sponsorisés" associés à des requêtes
  * Système d'enchères qui peut coûter très cher

* Les réseaux sociaux sont un nouveau point d'entrée à optimiser, mais c'est une autre histoire 


=== Passer à l'échelle ===

--- Problématiques du passage à l'échelle ---

* Un serveur peut gérer de 100 à 10000 clients simultanés
  * Comment faire quand comme facebook on a 500 millions d'utilisateurs simultanés ?
  * Limité par la puissance de calcul et la bande passante du réseau

* Perception du temps de réponse d'un site 
  * Au-delà de quelques secondes, la moitié des utilisateurs a fermé la fenêtre

* Enjeux
  * Répondre rapidement à un utilisateur où qu'il soit sur la planète
  * Minimiser le temps pendant lequel le service n'est pas disponible
  * Réduire les traitements côté serveur
  * Réduire la synchronisation nécessaire avec la base de données


--- Côté server vs côté client ---

* Ce cours s'intéresse surtout à enseigner le côté serveur
  * Le code exécuté côté serveur est digne de confiance (attention aux vulnérabilités du type injections SQL)
  * On ne contrôle pas du tout ce qui se passe côté client
  * Mais les nombreux traitements côté serveur empêchent de passer à l'échelle

* Single page applications (SPA)
  * L'aspect graphique est complètement géré par le client
  * Le serveur s'occupe principalement d'envoyer les scripts à exécuter côté client 
    * Le client accède aux données par une API REST
  * Sites très dynamiques, convergence avec ce qui se passe sur téléphone (frameworks comme React / React-Native)

* Applications progressives
  * Les sites SPA sont lents à charger
  * Le serveur envoie une page pré-rendue avec des fonctionnalités minimales à afficher le temps du chargement

  
--- Proximité du client ---

* Content Distribution Network (CDN)
  * Rapprocher le contenu statique des usagers
  * Mise en cache des téléchargements, vidéos, images, pages statiques, scripts client
  * Les plus connus : Akamai, Cloudflare, Cloudfront, les GAFAM...
    * Attention, certains se rémunèrent en analysant l'utilisation

[cdn.png]

* Rapprocher les serveurs des clients (partie dynamique)
  * Découpage du monde en régions (Afrique, Amérique, Asie, Europe, Océanie...)
    * On fait tourner une réplique du serveur dans chaque région
    * La base de donnée doit quand-même être centralisée ou synchronisée régulièrement (demande une séparation fine des données chaudes et froides)
  * Carte des datacenters \url{https://www.infrapedia.com}


--- Équilibrage de charge et haute disponibilité ---

* Problématique
  * La génération dynamique de page demande des ressources de calcul
  * Une machine physique ne peut gérer qu'un nombre limité de clients à la fois
  * Que se passe-t-il si un serveur plante, un disque dur tombe en panne ?
  * {\color{blue} Solution} : distribuer les requêtes sur plusieurs serveurs

\begin{columns}
\column{.5\textwidth}

* Algorithmes
  * Sélection aléatoire du serveur
  * Chacun son tour (Round-robin)
  * Le moins chargé (Least connections)
  * Clé de hachage liée à l'IP du client
  * {\color{red} Attention} : gestion de session

\column{.5\textwidth}

[120#load-balancing.pdf]

\end{columns}

* Implémentations populaires
  * HAProxy, nginx, keepalived
  * iptables, DNS

--- Montée en charge dynamique ---

* L'utilisation d'un site connait des changements plus ou moins forts
  * Augmentation progressive de popularité
  * Événement du monde réel (vacances, compétition sportive, élection...)
  * Publication d'un lien sur un site très populaire
  * Attaque de type déni de service distribué (faire appel à un service de protection contre ces attaques)

* Dimensionnement dynamique 
  * Mesure de la charge d'une machine virtuelle (nombre de connexions, CPU...)
  * Ajout / suppression automatique de machines virtuelles en fonction de la charge
  * Programmable en avance si on peut anticiper

* Proposé par les solutions de cloud (ex: Amazon elastic cloud computing)

=== Conclusion ===

--- Conclusion ---

* Les bases du déploiement
  * Acheter et configurer un nom de domaine
  * Générer un certificat auprès d'une autorité reconnue par les navigateurs
  * Configurer un reverse proxy

* Le déploiement et le passage à l'échelle de sites web sont complexes
  * Assurer la sécurité, maintenir à jour
  * Automatiser un maximum
  * Proximité = vitesse perçue

* Un marché énorme pour les fournisseurs de "cloud"
  * Toutes les étapes intermédiaires peuvent être monnayées
  * Attention au vendor lock-in (difficulté de changer de 

% vi:set spell spelllang=fr:
