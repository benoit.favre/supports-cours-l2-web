Lang: french
Title: [Web] Web côté client
Author: [B. Favre, F. Bechet] B. Favre, F. Bechet, \{prenom.nom\}@lif.univ-mrs.fr
Institute: [AMU] Aix-Marseille Université
Date: \today
Theme: Boadilla
Coloring: javascript
%Outline: Plan du cours

=== Introduction ===

%--- Vous avez vu auparavant ---

%* Rappels
%    * Tableaux
%    * Chaines
%    * Dictionnaires
%    * Structures de données
%    * Timers
%* jQuery
%    * Sélecteurs
%    * Création de contenu
%    * Lecture de contenu
%    * Changer le CSS
%    * Événements

--- Aujourd'hui vous allez voir ---

%* Asynchronie
%    * Callbacks
%    * Pièges
* Dessiner dans une page web
    * SVG
    * La balise canvas
    * Primitives
        * Rectangles
        * Chemins
        * Arcs
        * Courbes
    * Styles
    * Animation
    * Animations CSS

%--- TP galerie ---

%[90#galerie.png]

%=== Asynchronie ===
%
%--- Callbacks ---
%
%* En javascript, une fonction est une *valeur* comme un tableau, une chaîne ou un entier
%* On peut affecter une fonction à une variable
% %%% js
%var a = console.log;
%a('Hello world');
%a = function(arg) {
%    alert('your message is: ' + arg);
%}
%a('Hello again');
% %%%
%* Un callback est une fonction passée en argument d'une autre fonction
%* De manière générale, le callback est appelé lorsque le résultat est prêt
% %%% js
%function f(x, callback) {
%    callback(x + 1);
%}
%
%f(3, function(arg) { // fonction anonyme
%    console.log(arg);
%});
% %%%
%
%--- Asynchronie en javascript ---
%
%* Qu'est-ce qu'un programme asynchrone ?
%    * Plusieurs choses peuvent être effectuées en même temps
%    * Exemple : un programme C de première année ne fait qu'une chose à la fois
%    * Le navigateur charge plusieurs images en même temps
%
%* Que se passe-t-il lorsque l'un `timeout` arrive à échéance ?
%    * L'interpréteur javascript est *séquentiel*
%        * Avantage : sécurité plus simple
%    * Une seule fonction est exécutée à la fois
%    * Pendant l'exécution d'une fonction :
%        * L'affichage graphique n'est pas mis à jour
%        * Les fonctions événements sont en attente
%
% %%% js
%for(var i = 0; i < 1000000; i++) {
%    $(document.body).append('Iteration ' + i);
%}
% %%%
%
%--- Asynchronie : ce que ça implique ---
%
%Comment attendre dans une boucle ?
% %%% js
%// Ne marche pas
%for(var i = 10; i >= 0; i--) {
%    $('#result').text(i);
%    sleep(1);
%}
%
%// Il faut faire un callback récursif
%function compte_a_rebours(i) {
%    if(i >= 0) {
%        $('#result').text(i);
%        setTimeout(function() {
%            compte_a_rebours(i - 1);
%        }, 1000);
%    }
%}
%compte_a_rebours(10);
% %%%
%
%--- Ordre d'appel des callbacks ---
%
%* Un seul callback est exécuté à la fois, *après* la fonction courante
% %%% js
%setTimeout(function() {
%    console.log('never executed');
%}, 1000);
%while(true) { }
% %%%
%
%* Problème : quelle est la valeur des variables modifiées en dehors du callback ?
% %%% js
%for(var i = 0; i < 10; i++) {
%    setTimeout(function() {
%        console.log(i); // affiche 10 à chaque fois
%    }, 100);
%}
% %%%
%
%--- Ordre d'appel des callbacks (2) ---
%
%* Problème : comment récupérer le résultat de calculs asynchrones ?
% %%% js
%// on veut connaitre la largeur totale des images
%var total = 0;
%for(var i = 0; i < images.length; i++) {
%    images[i].onload = function() {
%        total += this.width;
%    }
%    images[i].src = 'images/' + i + '.png';
%}
%console.log(total); // affiche 0
% %%%
%
%--- Utilisation de variables externes ---
%
%* Quelle est la valeur des variables modifiées en dehors du callback ?
%* Solution : il faut "geler" les variables en les rendant locales
%    * Encapsulation de l'appel dans une fonction
% %%% js
%for(var i = 0; i < 10; i++) {
%    var f = function(frozen_i) {
%        setTimeout(function() {
%            console.log(frozen_i); // affiche les bonnes valeurs
%        }, 100);
%    };
%    f(i);
%}
% %%%
%
%--- Récupérer le cumul de résultats ---
%
%* Comment récupérer le résultat de calculs asynchrones ?
%* Solution : le résultat n'est disponible que lorsque tous les callbacks ont été appelés.
% %%% js
%var total = 0;
%var remaining = images.length;
%for(var i = 0; i < images.length; i++) {
%    images[i].onload = function() {
%        total += this.width;
%        remaining--;
%        if(remaining == 0) console.log(total);
%    }
%    images[i].src = 'images/' + i + '.png';
%}
% %%%

=== Dessiner avec SVG ===

--- Dessiner et animer en javascript ---

* SVG
    * Dessin vectoriel
    * On manipule les formes comme des éléments du DOM
* Canvas
    * On a enfin accès aux pixels !!!!
    * Permet le contrôle le plus fin
    * Le plus rapide lorsqu'on a beaucoup d'éléments graphiques
    * La 3D est a vous (mais on le verra pas dans ce cours)
* CSS
    * Pour faire des animations statiques (si ça a un sens)

--- SVG ---

* Scalable Vector Graphics
    * Dessin vectoriel (Inkscape)
    * Défini en XML
    * Existe indépendamment du html
    * Intégration facile au html
    * Les balises sont des noeuds dom

%%% html
<html>
<body>

<h1>Mon premier SVG</h1>

<svg width="100" height="100">
  <circle cx="50" cy="50" r="40" stroke="green" stroke-width="4" fill="yellow" />
</svg>

</body>
</html>
%%%

--- Qu'est-ce que le XML ? ---

* EXtensible Markup Language
    * Similaire à HTML
    * Définit de manière rigoureuse (toutes les balises doivent être fermées)
    * Nommage des balises libre
    * Description des balises dans une DTD (ou Schema)
    * Analyse et production dans de nombreux langages
    * xhtml est la version xml du html

%%% html
<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE racine SYSTEM "racine.dtd">
<racine>
    <!-- commentaire -->
    <element>Texte</element>
    <donnee attribut="valeur">
        <autofermant/>
        une entité &amp; une balise &lt;html&gt;
        idem pour les &quote;guillemets&quote;
    </donnee>
    <h:element xmlns:h="http://www.w3.org/TR/html4/">Élément ambigü</h:element>
    <exemple><![CDATA[  <html><body></body></html> ]]></exemple>
</racine>
%%%

--- Primitives SVG ---

%%% html
<svg>
<g transform="rotate(30)">
    <image xlink:href="firefox.jpg" x="0" y="0" height="50" width="50" />
    <circle cx="100" cy="100" r="20" fill="red" stroke-width="1" stroke-dasharray="5,5" />
    <line x1="10" y1="20" x2="30" y2="40" stroke="blue" />
    <rect width="300" height="100" style="fill:rgb(0,0,255);stroke:rgb(0,0,0)" />
    <text x="40" y="40">Bonjour</text>
<g>
</svg>
%%%

* Définition des attributs
    * Les transformations, traits, remplissage... sont hérités de `<g>` 
* Intégration avec html
    * On peut styler les éléments avec CSS (class, id...)
    * On peut ajouter des événements %jquery

--- Horloge animée (svg)

%<script src="jquery-2.1.0.min.js"></script>
%%% html
<html> <head>
<script>
function move_hand(id, angle) {
    document.getElementById(id).setAttribute('transform', 'rotate(' + angle + ')');
}
function draw() {
    var now = new Date();
    move_hand('hours', 360 / 12 * now.getHours());
    move_hand('minutes', 360 / 60 * now.getMinutes());
    move_hand('seconds', 360 / 60 * now.getSeconds());
} 
setInterval(draw, 1000);
</script>
</head> <body>
<svg width="100" height="100">
    <circle cx="50" cy="50" r="49" fill="white" stroke="black"/>
    <g transform="translate(50,50)">
        <line x1="0" y1="0" x2="0" y2="-30" stroke="black" stroke-width="3" id="hours" />
        <line x1="0" y1="0" x2="0" y2="-40" stroke="blue" stroke-width="2" id="minutes" />
        <line x1="0" y1="0" x2="0" y2="-50" stroke="red" stroke-width="1" id="seconds" />
    </g>
</svg>
</body> </html>
%%%

=== Le canvas ===

--- La balise canvas ---

%%% html
<canvas width="300" height="150">
    La balise canvas n'est pas supportée
</canvas>
%%%
* La balise canvas se comporte comme une image mais n'a pas de source.
* Si elle n'est pas supportée, le texte est affiché (`</canvas>` est requis)
* Tant qu'on ne dessine rien dedans, elle est transparente
* `width` et `height` déterminent le nombre de pixels du canvas
* Sa taille effective peut être différente (fixée par css)

--- Le contexte ---

Pour dessiner, il faut un contexte
%%% js
var canvas = document.getElementsByTagName('canvas')[0]; // noeud dom
canvas.width = 400;
canvas.height = 100;
var context = canvas.getContext('2d');
context.fillText('Bonjour monde', 20, 20);
context.fillStyle = 'red';
context.fillText('Bonjour monde', 40, 40);
%%%

--- Dessiner un rectangle ---

* `fillRect` : dessine un rectangle plein
* `strokeRect` : dessine la bordure d'un rectangle
* `clearRect` : rend transparente une partie rectangulaire du canvas

%%% js
var ctx = canvas.getContext("2d");

ctx.fillStyle = "rgb(200,0,0)";
ctx.fillRect (10, 10, 55, 50);

ctx.fillStyle = "rgba(0, 0, 200, 0.5)";
ctx.fillRect (30, 30, 55, 50);
%%%

--- Dessiner des traits ---

L'approche générale est de dessiner des polygones
* `.beginPath()` : commence un polygone
* `.moveTo(x,y)` : positionner le début d'une ligne
* `.lineTo(x,y)` : ajouter un segment du début courant jusqu'au point spécifié
* `.closePath()` : ajouter un segment qui revient au début du polygone
* `.stroke()` : dessiner le contour du polygone
* `.fill()` : remplir le polygone (appel implicite à `closePath`)

Exemple :
%%% js
var ctx = document.getElementsByTagName('canvas')[0].getContext('2d');
ctx.beginPath();
ctx.moveTo(20, 20);
ctx.lineTo(40, 40);
ctx.lineTo(60, 20);
ctx.lineTo(80, 40);
ctx.lineTo(100, 20);
ctx.closePath();
ctx.stroke();
%%%

--- Arcs et cercles ---

* Dessiner un arc
%%% js
var ctx = document.getElementsByTagName('canvas')[0].getContext('2d');
// arc(x, y, rayon, angle_debut, angle_fin)
ctx.beginPath();
ctx.arc(30, 30, 20, Math.PI / 4, Math.PI / 2);
ctx.fill();
%%%

* Dessiner un cercle (angle de $0$ à $2\pi$)
%%% js
var ctx = document.getElementsByTagName('canvas')[0].getContext('2d');
ctx.beginPath();
ctx.arc(30, 30, 40, 0, 2 * Math.PI);
ctx.stroke();
%%%

--- Texte ---

On peut aussi dessiner du texte
* `.font` : spécifie la taille et le type de la police
* `.fillStyle` : spécifie la couleur
* `.fillText(text, x, y)` : dessine le texte

Spécifier l'ancrage du texte par rapport à $(x, y)$
* `.textAlign = 'left|right|center|start|end'` : alignement horizontal
* `.textBaseline = 'top|bottom|middle|alphabetic|hanging` : alignement vertical

Mesurer la taille du texte
* `.measureText(texte)` : retourne la longueur en pixels du texte

%%% js
var ctx = document.getElementsByTagName('canvas')[0].getContext('2d');
ctx.font = 'bold 20px arial';
ctx.fillText('Bonjour monde', 30, 30);
%%%

--- Dessiner une image ---

Dessiner une image sur le canvas
* À partir de l'élément `image`, `video`, `canvas`
%%% js
var ctx = document.getElementsByTagName('canvas')[0].getContext('2d');
var image = document.getElementById('video')[0];
ctx.drawImage(image, 30, 20); // image, x, y
%%%

* Charger une image sans l'ajouter au DOM
%%% js
var image = new Image();
image.onload = function() {
    var ctx = document.getElementsByTagName('canvas')[0].getContext('2d');
    ctx.drawImage(image, 0, 0);
}
image.src = 'https://www.google.com/images/srpr/logo11w.png'
%%%

* On peut redimensionner l'image, ou n'en dessiner qu'une partie
%%% js
ctx.drawImage(image, x, y, largeur, hauteur);
ctx.drawImage(image, sx, sy, sl, sh, dx, dy, dl, dh); // s = source, d = destination
%%%

--- Couleur et style ---

* On peut changer la couleur par `.fillStyle` et `.strokeStyle` :
%%% js
ctx.fillStyle = "orange"; // couleur nommée
ctx.fillStyle = "#FFA500"; // couleur html
ctx.fillStyle = "rgb(255,165,0)"; // rouge, vert, bleu [0-255]
ctx.fillStyle = "rgba(255,165,0,1)"; // RVB + transparence [0-255]
%%%

* Changer la transparence des images (ou de tout ce qui est dessiné)
%%% js
ctx.globalAlpha = 0.5; // valeur entre 0 et 1
ctx.drawImage(image, 0, 0);
%%%

* Types de ligne
%%% js
ctx.lineWidth = 5;     // change l'épaisseur de la ligne
ctx.lineCap = 'round'  // début et fin de ligne 'butt|round|square'
ctx.lineJoin = 'round' // liaison entre deux lignes 'round|bevel|mitter'
%%%

%--- Transparence ---

%--- Gradients ---

%--- Patterns ---

--- Transformations ---

Sauvegarde de l'état du canvas (couleur, transparence...)
%%% js
ctx.fillStyle = 'blue';
ctx.save();
ctx.fillStyle = 'red';
ctx.restore(); // fillStyle = 'blue'
%%%

On peut appliquer des transformations au système de coordonnées
%%% js
ctx.translate(x, y); // translation de l'origine
ctx.rotate(angle); // rotation autour de l'origine dans le sens horaire
ctx.scale(sx, sy); // changement de l'échelle des axes

// exemple
ctx.translate(100, 100);
ctx.scale(30, 30);
ctx.arc(0, 0, 1, 0, 2 * Math.PI);
ctx.fill(); // rempli un cercle de centre 100, 100 et de rayon 30
%%%

--- Autres possibilités ---

\begin{columns}
\column{.5\textwidth}

* Courbes de bézier : courbes décrites par des points de contrôle
[20#bezier.png]
* Dégradés de couleurs linéaires ou radials
[20#gradient.png]
* Mosaïques : juxtapose des images pour créer un pavage
[20#tiles.png]

\column{.5\textwidth}

* Composition : affiche une image en fonction des pixels d'en dessous
[20#composite.png]
* Masquage : seuls les éléments dans le masque sont affichés
[20#clipping.png]

\end{columns}

--- Animations ---

* Idée : utiliser `setInterval` pour redessiner le canvas à intervalles réguliers
%%% js
var canvas = document.getElementsByTagName('canvas')[0];
var ctx = canvas.getContext('2d');
var x = 0;
function redraw() {
    x = (x + 3) % canvas.width;
    ctx.clearRect(0, 0, canvas.width, canvas.height);
    ctx.fillRect(x, 30, 20, 20);
}
setInterval(redraw, 40); // environ 25 fois par seconde
%%%

* Éviter les scintillements
%%% js
function redraw(time) { // time est très précis
    ctx.drawText(time, 30, 30);
    window.requestAnimationFrame(redraw);
}
var id = window.requestAnimationFrame(redraw);
// annulé avec window.cancelAnimationFrame(id);
%%%

--- Horloge animée (canvas) ---

# `new Date()` renvoie une date avec `.getHours()`, `.getMinutes()` et `.getSeconds()`.
# Utiliser `rotate(angle)` pour afficher les aiguilles des heures, minutes secondes
# Dessiner une ligne du centre pour chaque aiguille
# Utiliser `setInterval` pour mettre à jour l'horloge toutes les secondes

%%% js
var ctx = document.getElementsByTagName('canvas')[0].getContext('2d');
function draw_hand(ctx, angle, width, length, color) {
    ctx.save(); ctx.translate(50, 50); ctx.rotate(angle);  
    ctx.beginPath(); ctx.moveTo(0, 0); ctx.lineTo(0, -length);
    ctx.strokeStyle = color; ctx.lineWidth = width; ctx.stroke();
    ctx.restore(); 
}
function draw() {
    var now = new Date();
    ctx.clearRect(0, 0, 100, 100); 
    ctx.beginPath(); ctx.arc(50, 50, 50, 0, 2 * Math.PI); ctx.stroke();
    draw_hand(ctx, 2 * Math.PI / 12 * now.getHours(), 3, 30, 'black');
    draw_hand(ctx, 2 * Math.PI / 60 * now.getMinutes(), 2, 40, 'blue');
    draw_hand(ctx, 2 * Math.PI / 60 * now.getSeconds(), 1, 50, 'red');
}   
setInterval(draw, 1000);
%%%

--- Récupérer les coordonnées de la souris ---

* En DOM :
%%% js
function get_mouse(event) {
    var rect = document.getElementById('canvas').getBoundingClientRect();
    var x = event.clientX - rect.left;
    var y = event.clientY - rect.top;
    return [x, y];
}
%%%

%* En jQuery :
% %%% js
%function get_mouse(event) {
%    var position = document.getElementsByTagName('canvas').offset();
%    return {x: event.pageX - position.left, y: event.pageY - position.top};
%}
% %%%

--- Dessiner sur le canvas ---

%%% js
function get_mouse(event) {
    var rect = document.getElementById('canvas').getBoundingClientRect();
    return {x: event.clientX - rect.left, y: event.clientY - rect.top};
}
var canvas = document.getElementsByTagName('canvas')[0];
var ctx = canvas.getContext('2d');
var point = {x: 0, y: 0};
var drawing = false;
canvas.mousedown = function(event) {
    point = get_mouse(event);
    drawing = true;
};
canvas.mouseup = function(event) {
    drawing = false;
};
canvas.mousemove = function(event) {
    next = get_mouse(event);
    ctx.beginPath();
    ctx.moveTo(point.x, point.y);
    ctx.lineTo(next.x, next.y);
    ctx.stroke();
    point = next;
};
%%%

=== Animations CSS ===

--- Animations en CSS ---

* Sans une seule ligne de javascript
    * Certains événements sont supportés (`:hover`)
* Toutes les propriétés peuvent être animées
    * Position, couleur, bordure, etc.
* Sur les vieux navigateurs, il faut utiliser un préfixe pour les nouveaux attributs CSS
    * Firefox: -moz-animation-name
    * Chrome: -webkit-animation-name
    * Opera: -o-animation-name
* Normalement, sur un navigateur récent, ce n'est pas nécessaire

--- Definir des keyframes ---

* La règle `@keyframes` permet de créer une animation
* Celle ci doit être utilisée depuis un élément

%%% css
@keyframes bouge {
    from { left: 20%; }
    to { left: 80%; }
}
div {
    width: 100px;
    height: 100px;
    background-color: red;
    animation-name: bouge;
    animation-duration: 4s;
} 
%%%

--- Keyframes (2) ---

* On peut spécifier les keyframes en pourcentage du temps d'animation
	* `from = 0\%`
	* `to = 100\%`

%%% css
@keyframes couleur {
    0%   {background-color: red; left: 20%}
    20%  {background-color: orange;}
    40%  {background-color: yellow;}
    60% {background-color: green;}
    80% {background-color: blue;}
    100% {background-color: purple; left: 80%}
}

div {
    width: 100px;
    height: 100px;
    position: fixed;
    background-color: red;
    animation-name: couleur;
    animation-duration: 8s;
    animation-direction: alternate;
    animation-iteration-count: infinite;
    animation-timing-function: linear;
}
%%%

--- Attributs ---

* `animation-name` : nom de la règle spécifiant les keyframes
* `animation-delay` : délais avant le début de l'animation
* `animation-direction` : direction de l'animation (`normal`, `alternate`, `reverse`)
* `animation-duration` : durée en secondes
* `animation-iteration-count` : nombre d'itérations à effectuer (`infinite` = ne jamais s'arrêter)
* `animation-timing-function` : fonction qui gère la vitesse d'animation
    * `linear` : linéaire
    * `ease` : accélère et ralenti (`ease-in`, `ease-out`, `ease-in-out`)
    * `steps(int)` : nombre d'image saccadées
    * `cubic-bezier(n,n,n,n)` : définition fine de la fonction

--- Bilan de la séance ---

%* Asynchronie
%    * Callbacks
%    * Pièges
* Dessiner dans une page web
    * SVG
    * La balise canvas
    * Primitives
        * Rectangles
        * Chemins
        * Arcs
        * Courbes
    * Styles
    * Animation
    * Animations CSS

% vi:set spell spelllang=fr:
