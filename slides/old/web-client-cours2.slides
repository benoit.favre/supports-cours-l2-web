Lang: french
Title: [Web] Web côté client
Author: [B. Favre, F. Bechet] B. Favre, F. Bechet, \{prenom.nom\}@lif.univ-mrs.fr
Institute: [AMU] Aix-Marseille Université
Date: \today
Theme: Boadilla
Coloring: javascript
%Outline: Plan du cours

=== jQuery ===

--- Vous avez vu auparavant ---

* Retour sur les acquis en javascript
    * Les tableaux, dictionnaires, structures, objets, chaînes de caractères, fonctions
    * Ranger un tableau
    * CSS (changer le style depuis javascript)
* Introduction à HTML5
    * Qu'est-ce que HTML5
    * Squelette d'une page web
    * Tour d'horizon des balises
* Les formulaires HTML5
    * Principe
    * Champs : types et étiquettes
    * Boutons
    * Formulaires en javascript
* Structures et objets
    * Sérialisation JSON
    * Stockage dans le navigateur

--- Aujourd'hui vous allez voir ---

* jQuery
    * Sélecteurs
    * Création de contenu
    * Lecture de contenu
    * Changer le CSS
    * Événements
* Callbacks
    * setTimeout / setIntervall
    * Asynchronie
    * Les pièges à éviter
* HTML5
    * La balise audio

--- jQuery ---

* Objectif : simplifier le javascript
    * Traitements par lots
    * Sélection d'éléments du DOM
    * Compatible avec de nombreux navigateurs
* Créé en 2006
    * http://www.jquery.com
* Utilisé dans de grands sites web
    * Wordpress, mozilla, IBM, Intel, Adobe, Blackberry, Github
    * 80\% des 10000 sites web les plus fréquentés
* Communauté très active
    * Forums
    * Plus de 2000 plugins
* Avantages
    * Très léger environ 20k
    * Interface utilisateur
    * Navigateur mobile

--- Comparaison avec DOM ---

* Mettre en rouge la police des cellules de quelques tableaux de résultats
%%% js
var resultats = document.getElementsByClassName('resultats');
for(var r = 0; r < resultats.length; r++) {
    var td = resultats[r].getElementsByTagName('td');
    for(var i = 0; i < td.length; i++) {
        td[i].style.color = 'red';
    }
}
%%%

* La même chose en jQuery
%%% js
$('.resultats td').css('color', 'red');
%%%

--- Le principe général ---

%%% js
$('div').addClass('special');
%%%

# Sélectionner des éléments
    * Sélecteurs similaire à CSS
    * `$(<selecteur>)` renvoie un tableau de noeuds DOM
# Faire quelque chose avec eux
    * `.fonction(arguments)` applique la fonction à tous les éléments sélectionnés
    * Si aucun élément est sélectionné, rien ne se passe

* `$()` est la fonction à tout faire
%%% js
$('div')         // sélectionne des éléments
$(document.body) // convertit un nœud DOM en jQuery
$(function() {}) // exécute la fonction au chargement de la page
%%%

--- Première page jQuery ---

%%% html
<html>
<head>
    <meta charset="UTF-8">
    <!-- inclusion directe depuis le site de jQuery (CDN) -->
    <script src="http://code.jquery.com/jquery-2.1.0.min.js"></script>
    <script>
        $(function() {
            // la page est prête
            $('div').text('bonjour');
        });
    </script>
</head>
<body>
    <div>rien pour l'instant</div>
</body>
</html>
%%%

--- Sélecteurs ---

* Sélecteurs basiques
%%% js
$('*') // tous les noeuds DOM
$('tag') // renvoie tous les élément d'un type donné de la page
$('#element') // renvoie l'élément d'identifiant 'element'
$('.classe') // renvoie tous les éléments d'une classe donnée
%%%

* Multicritères
%%% js
$('selecteur1, selecteur2, selecteur3') // OU logique entre les sélecteurs
$('ancetre descendant') // cherche les parents, puis renvoie les descendants de chacun de ces parents
$('parent > enfant') // renvoie les fils directs de chaque parent sélectionné
$('.classe1.classe2.classe3') // renvoie les éléments qui ont toutes les classes
%%%

--- Sélection sur les attributs ---

* `$("element[nom='valeur']")`
    * `name!='valeur'` different de
    * `name*='valeur'` contient
    * `name~='valeur'` contient un mot
    * `name^='valeur'` commence par
    * `name$='valeur'` fini par

Exemples :
%%% js
$("div[id='toto']")
%%%

Exemple : tous les liens https
%%% js
$("a[href^='https://']")
%%%

--- Sélection des noeuds autour ---

Les noeuds parents des noeuds sélectionnés
%%% js
$('div').parent()
%%%

* `.next()` : suivant
* `.previous()` : précédent
* `.children()` : tous les enfants
* `.siblings()` : les frères et soeurs

* Toutes les fonctions retournent le tableau des éléments sélectionnés
    * Donc on peut effectuer plusieurs traitements à la chaîne
%%% js
$('div').parent().css('color', 'red').siblings().css('color', 'green');
%%%

--- Autres selecteurs ---

Modifieurs de sélection
%%% js
$('div:first') // la première div de la page
%%%

* `:last` : dernier élément
* `:even` : éléments pairs
* `:odd` : éléments impairs
* `:first-child` : premier fils
* `:last-child` : dernier fils
* `:has(selector)` : éléments dont les fils correspondent à un critère
* `:visible` : éléments visibles
* `:hidden` : éléments invisibles
* `:empty` : élément vide
 
Documentation complète : http://www.w3schools.com/jquery/jquery_ref_selectors.asp

--- Lire le contenu des éléments ---

* Récupérer le contenu html des éléments sélectionnés
%%% js
console.log($('div').html());
%%%

* `.html()` : récupère la concaténation du contenu html des éléments
* `.text()` : récupère juste le texte
* `.val()` : les valeurs des `<input>` sélectionnés
* `.attr('name')` : l'attribut `name`

--- Changer le contenu des éléments ---

* On passe en argument le nouveau texte
%%% js
$('div').text('coucou'));
%%%

* `.html(content)` : change le contenu html (comme `.innerHTML`)
* `.text(content)` : change le texte
* `.val(content)` : change la valeur des `<input>` sélectionnés
* `.attr('name', value)` : l'attribut `name`
    * On peut changer plusieurs attributs à la fois
%%% js
$('img').attr({src: 'image.png', width: 200, height: 300});
%%%

--- Changer le CSS ---

* Changer un attribut css : `$('selector').css('name', 'value')`
%%% js
$('table').css('border', '2px solid blue');
$('tr td.important').css('font-weight', 'bold');
$('a').css('text-decoration', 'none');
%%%
* Attributs multiples d'un coup
%%% js
$('div').css({
    font_size: '12px',
    font_variant: 'small-caps',
});
%%%
* Cacher un élément
    * `$('div').hide()` : fait disparaitre l'élément
    * `$('td').show()` : fait apparaitre l'élément
    * `$('img').toggle()` : alterne entre apparition et disparition

--- Construction de nouveaux éléments ---

* Ajouter du contenu
    * `.after(contenu)` : ajouter le contenu après chaque élément 
    * `.before(contenu)` : ajouter avant chaque élément
    * `.append(contenu)` : ajouter à la fin des fils
    * `.prepend(contenu)` : ajouter avant le premier fils

* Comment créer du contenu
%%% js
var texte = "<p>Coucou</p>"; // créer du contenu html
var jquery = $("<p></p>").text("Coucou"); // créer avec jQuery
var dom = document.createElement("p"); // créer avec DOM
dom.innerHTML = "Coucou";
$("p").append(texte, jquery, dom); // ajout à chaque paragraphe
%%%

* Supprimer le contenu
    * `$('div').remove()` : supprime les éléments sélectionnés
    * `$('div').empty()` : supprime tous les fils

--- Exemple : créer une table ---

%%% js
var data = [{name: 'Pierre', age: 12}, 
    {name: 'Paul', age: 7}, {name: 'Jacques', age: 9}];

var table = $('<table>');
for(var i = 0; i < data.length; i++) {
    var row = $('<tr>');
    row.append($('<td>').text(data[i].name));
    row.append($('<td>').text(data[i].age));
    table.append(row);
}
$(document.body).append(table);
table.attr('border', 1);
$('tr:odd').css('background-color', 'gray');
%%%

--- Modifier les classes des éléments ---

%%% html
.important {
    color:red;
}
.debut {
    border-top: 3px solid black;
}
%%%

%%% js
$('div').addClass('important'); // ajoute une classe
$('.important').removeClass('important'); // supprime la classe
$('div').toggleClass('debut'); // faire alterner la classe
%%%

--- Traitements génériques sur les noeuds ---

* Parfois, on veut exécuter un traitement sur tous
les éléments sélectionnés.
%%% js
$('td').each(function(index, node) {
    console.log(index, node)
    $(node).text("élément " + index);
});
%%%

* Attention : les objets jQuery n'ont pas les mêmes fonctions que les objets DOM.
%%% js
// Récupération de l'objet DOM :
var dom_node = $('#element')[0];
dom_node.lastChild.style.background_color = "green";

// Repassage en mode jQuery
$(dom_node).css('color', '#ffeeaa');
%%%

--- Événements ---

* Patron général
%%% js
// Ajouter un callback en cas de clic
$('div').click(function(event) {
    // action
    alert('youpi on a cliqué');
    $(this).css('background-color', 'red');
});

// Simuler un clic
$(selector).click();
%%%

Événements supportés
* Souris : click, dblclick, mouseenter, mouseleave, mousedown, mouseup
* Clavier : keypress, keydown, keyup
* Formulaires : submit, change, focus, blur
* Document : load, resize, scroll, unload

--- Timers ---

* Appelle une fonction après un délai de temps
%%% js
var id = setTimeout(function() {
    console.log('time ran out');
}, 3000);
%%%
* Appelle une fonction à intervalles réguliers
%%% js
var i = 0;
var id = setInterval(function() {
    console.log('tick ' + i);
    i++;
}, 1000);
%%%
* Annulation
%%% js
clearTimeout(id);
clearInterval(id);
%%%
* Les temps sont en millisecondes
* Comment simuler un intervalle avec des timeout ?

--- Callbacks ---

* En javascript, une fonction est une *valeur* comme un tableau ou un entier
* On peut affecter une fonction à une variable
%%% js
var a = console.log;
a('Hello world');
a = function(arg) {
    alert('your message is: ' + arg);
}
a('Hello again');
%%%
* Un callback est une fonction passée en argument d'une autre fonction
* Le callback est appelé lorsque le résultat est prêt
%%% js
function f(x, callback) {
    callback(x + 1);
}

f(3, function(arg) { // fonction anonyme
    console.log(arg);
});
%%%

--- Asynchronie en javascript ---

* Qu'est-ce qu'un programme asynchrone ?
    * Plusieurs choses peuvent être effectuées en même temps
    * Exemple : un programme C de première année ne fait qu'une chose à la fois
    * Le navigateur charge plusieurs images en même temps

* Que se passe-t-il lorsque l'un `timeout` arrive à échéance ?
    * L'interpréteur javascript est *séquentiel*
        * Avantage : sécurité plus simple
    * Une seule fonction est exécutée à la fois
    * Pendant l'exécution d'une fonction :
        * L'affichage graphique n'est pas mis à jour
        * Les fonctions événements sont en attente

%%% js
for(var i = 0; i < 1000000; i++) {
    $(document.body).append('Iteration ' + i);
}
%%%

--- Asynchronie : ce que ça implique ---

Comment attendre dans une boucle ?
%%% js
// Ne marche pas
for(var i = 10; i >= 0; i--) {
    $('#result').text(i);
    sleep(1);
}

// Il faut faire un callback récursif
function compte_a_rebours(i) {
    if(i >= 0) {
        $('#result').text(i);
        setTimeout(function() {
            compte_a_rebours(i - 1);
        }, 1000);
    }
}
compte_a_rebours(10);
%%%

--- Ordre d'appel des callbacks ---

* Un seul callback est exécuté à la fois, *après* la fonction courante
%%% js
setTimeout(function() {
    console.log('never executed');
}, 1000);
while(true) { }
%%%

* Quelle est la valeur des variables modifiées en dehors du callback ?
%%% js
for(var i = 0; i < 10; i++) {
    setTimeout(function() {
        console.log(i); // affiche 10 à chaque fois
    }, 100);
}
%%%

--- Ordre d'appel des callbacks (2) ---

* Comment récupérer le résultat de calculs asynchrones ?
%%% js
// on veut connaitre la largeur totale des images
var total = 0;
for(var i = 0; i < images.length; i++) {
    images[i].onload = function() {
        total += this.width;
    }
    images[i].src = 'images/' + i + '.png';
}
console.log(total); // affiche 0
%%%

--- Utilisation de variables externes ---

* Quelle est la valeur des variables modifiées en dehors du callback ?
* Solution : il faut "geler" les variables en les rendant locales
    * Encapsulation de l'appel dans une fonction
%%% js
for(var i = 0; i < 10; i++) {
    var f = function(frozen_i) {
        setTimeout(function() {
            console.log(frozen_i); // affiche les bonnes valeurs
        }, 100);
    };
    f(i);
}
%%%

--- Récupérer le cumul de résultats ---

* Comment récupérer le résultat de calculs asynchrones ?
* Solution : le résultat n'est disponible que lorsque tous les callbacks ont été appelés.
%%% js
var total = 0;
var remaining = images.length;
for(var i = 0; i < images.length; i++) {
    images[i].onload = function() {
        total += this.width;
        remaining--;
        if(remaining == 0) console.log(total);
    }
    images[i].src = 'images/' + i + '.png';
}
%%%

--- La balise audio ---

La balise `audio` permet de jouer un son dans une page web
%%% html
<audio controls>
<source src="horse.ogg" type="audio/ogg">
<source src="horse.mp3" type="audio/mpeg">
    Votre navigateur ne supporte pas la balise
</audio>
%%%

Problème : les formats supportés
\begin{center}
\begin{tabular}{llll}
\hline
Navigateur          & MP3      & Wav    & Ogg \\
\hline
Internet Explorer   & oui      & non    & non \\
Chrome              & oui      & oui    & oui \\
Firefox             & bientôt  & oui    & oui \\
Safari              & oui      & oui    & non \\
Opera               & non      & oui    & oui \\
\hline
\end{tabular}
\end{center}

--- Balise audio : attributs ---

Attributs :
* `autoplay` : joue l'audio dès qu'il est chargé (musique de fond)
* `controls` : affiche un bouton stop/play et une barre de progression
* `loop` : joue le morceau en boucle
* `muted` : pas de son au démarrage
* `preload` (=auto, metadata, none) : précharge le contenu ou non
* `src` : url du fichier audio
* `<source src="chemin" type="type-mime">` : formats alternatifs (audio/mpeg=MP3, audio/ogg=Ogg, audio/wav=Wave)

Programmation en javascript
%%% js
var audio = $('#musique')[0];
audio.src = 'sound.mp3';
audio.load() // forcer le chargement
audio.play() // lancer la lecture
setTimeout(function() {
    audio.pause() // pauser la lecture
}, 10000);
%%%
Evenements DOM : (onplay, onplaying, onprogress...)

--- TP : séquenceur audio ---

[90#sequenceur.png]

% vi:set spell spelllang=fr:
