Lang: french
Date: 2020-2021
Title: [Web] Web : node-js \\ Licence Informatique L2
Author: [B. Favre] Benoit Favre
Institute: [AMU] Aix Marseille Université
Theme: Boadilla
Coloring: javascript


--- Projet ---

* Sujet libre, utilisant ce qu'on a vu dans le cours, contraint par les caractéristiques suivantes
  * Serveur web node.js / express
  * Base de données SQLite
    * Au moins trois tables
    * Possibilité de saisir des données depuis le site
  * Gestion des utilisateurs
    * Possibilité de créer, supprimer, se connecter, se déconnecter
    * Au moins deux rôles d'utilisateurs connectés
    * Conforme à la RGPD 
  * Séparation Modèle-Vue-Controlleur
    * Vues avec des templates
    * Bootstrap
    * Formulaires
  * Pas ou peu de javascript côté client

* Thème pour le projet : {\color{purple} "Rendre le monde meilleur" }

--- Projet ---

* Calendrier
  * 17 mars : annonce du projet
  * 24 mars : récolte des binômes et sujets choisis
    * Les enseignants valident la portée du sujet choisi : ni trop facile ni trop ambitieux
  * 31 mars : début du travail
  * 21 avril : rendu et évaluation du projet

* Critères d'évaluation
  * Respect des contraintes sujet
  * Originalité
  * Utilité
  * Aboutissement
  * Maîtrise et qualité du code

--- Choix du sujet ---

À faire avant le 23 mars
* Trouver un binôme (pas de trinôme possible)
* Choisir un sujet
  * Description fonctionnelle des éléments qui seront développés
  * Description technique : tables sql, vues, routes, etc.
  * Planning prévisionnel
* URL des sources du projet
  * github/gitlab ou glitch ou autre plateforme
  * Vous devrez y mettre les source au fur et à mesure de votre avancement
* Répondre au questionnaire Amétice
* Un sujet de "secours" sera donné très prochainement

=== Session ===

--- Sessions : Principes fondamentaux ---

* Constat
  * HTTP n'est pas un protocole connecté
    * Protocole sans état (stateless)
  * Optimisé pour le passage à l'échelle
    * Minimiser la mémoire nécessaire à une connexion
    * Tout oublier entre deux connexions
* À la charge au client de rappeler au serveur 
  * Qui il est
  * Et pourquoi il est là

* Est-ce réellement un problème ?
  * Déjà véhiculé en partie par les URL
  * Nécessiter de gérer une session

%--- Cache du navigateur ---

--- Définition d'une session ---

* Qu'est-ce qu'une session
  * Période pendant laquelle l'utilisateur est devant le site
  * Commence à l'ouverture d'une page du site
  * Prend fin à la fermeture de la page ou après un délais
    * Le client ne termine pas la session explicitement
    * Le client peut être mis en veille, se retrouver hors de portée du réseau...

* Données spécifiques à l'utilisateur
  * Maintenir l'état lié à la session
  * Volatilité : les données ne sont intéressantes que pendant la durée de l'interaction
    * À opposer aux données gardées dans la BDD
  * Peu de données (typiquement juste l'identifiant de l'utilisateur)

* Identifiant de session
  * Associé à une date de création
  * Et aux données utilisateur

--- Comment assurer la continuité ? ---

* L'identifiant de session est la seule information à garder tout au long de la session
  * Le serveur peut garder les infos associées séparément, par exemple en RAM

* Où garder cette information ?
  * Dans l'url :
    * `http://www.gogle.com?session=12391248`
  * Par l'intermédiaire des formulaires :
    * `<input type="hidden" name="session" value="12391248">`
  * Dans les cookies
    * Entêtes html : `Set-Cookie: session=12391248`

* Assurer la sécurité de l'identifiant de session
  * Il ne doit pas être possible d'usurper la session d'un autre utilisateur
  * Restriction de la durée de vie
  * Allocation aléatoire
  %* Signature cryptographique (même principe que TLS/SSL)

--- Où stocker l'information de session ? ---

* En mémoire
  * Problème : que faire en cas d'interruption du serveur ?
  * Comment gérer l'exhaustion de la mémoire ?

%%% js
var session = Math.random();
data[session] = {
  user: get_user(name, password),
  timestamp: new Date(),
}
%%%

* Dans une base de données
  * Accès très fréquent (à chaque page)
  * Nécessite une base rapide
  * Est-ce que ça vaut le coup sachant la volatilité des données

* Dans un Cookie
  * Mais qu'est-ce qu'un cookie ?

--- Cookies ---

* Information envoyée par l'intermédiaire des entêtes HTTP
  * `Set-Cookie: nom=valeur; nom2=value2;` 
  * Fixé par le serveur quand il répond à une requête
  * Mémorisé par le navigateur (client)
  * Renvoyé à chaque requête

* Durée de vie
  * Si non spécifié, le cookie est supprimé à la fermeture de la fenêtre
  * `Expires=date` : proposer une date d'expiration
  * `Max-Age=durée` : proposer un age maximum
  * Le navigateur décide _in fine_

* Options
  * Secure : uniquement envoyé en https
  * HttpOnly : ne peut être manipulé par js côté client
  * Domain : hôtes qui reçoivent le cookie
  * Path : chemin dans l'url associé au cookie
  * SameSite : envoyé uniquement pour une page sur le même site
  * Documentation : \url{https://developer.mozilla.org/fr/docs/Web/HTTP/Cookies}

{\s

\begin{verbatim}
set-cookie __Secure-3PSIDCC=AJi4QfG2cQ7SDfAQv4dWHAEVt0XkoLKowf0OfodCFFGY2_wx0KUbastOLxpOyAIxFsm-HZgZWm8; 
expires=Fri, 11-Mar-2022 16:16:56 GMT; path=/; domain=.google.com; Secure; HttpOnly; priority=high; SameSite=none
\end{verbatim}

}

--- Lire / écrire un cookie ---

* Nodejs
%%% js
var http = require('http');

http.createServer((req, res) => {
  console.log(req.headers['Cookie']);

  response.writeHead(200, {
    'Set-Cookie': 'moncookie=valeur; Expires=Wed, 11 Mar 2020 11:59:59 GMT',
    'Content-Type': 'text/html'
  });

  response.end('<strong> Hello World </strong>');
}).listen(3000);
%%%

--- Lire / écrire un cookie (2) ---

* Express
%%% js
var express = require('express');
var cookieParser = require('cookie-parser');

var app = express();
app.use(cookieParser());

app.get('/', function (req, res) {
  console.log('Cookies: ', req.cookies)
  res.cookie('nom', 'valeur', {maxAge: 1000 * 60 * 15});
  res.send('result');
})

app.listen(3000)
%%%

--- Cookies tiers ---

* Scénario
  # Vous vous connectez sur facebook: ajout d'un cookie unique
%* Facebook ajoute un cookie qui vous authentifie
  # Vous allez sur un autre site web qui affiche une image "like" de facebook
%# Ce site affiche une image "like" de facebook
  # Facebook récupère le cookie et sait de quel site il vient, ou peut exploiter votre IP
    * Entête "Referer" 

* Cookies tiers
  %* Aucune différence avec les cookies normaux
  * Arrive lorsqu'on charge une ressource depuis un autre site web (script, image...)
  * La base du traçage des utilisateurs

* Comment s'en prémunir
  * Bloqué par défaut dans Firefox récent  
  * Configuration du navigateur "Bloquer les cookies tiers"
    * Problème : casse certaines fonctionnalités
  * Extensions des navigateurs

* Mais il existe plein d'astuces pour faire des "super/ever" cookies
  * Plugins comme Flash (RIP), images invisibles, polices de caractères...
  * \url{https://amiunique.org/fp}; \url{https://coveryourtracks.eff.org}


--- Session : cookie crypté ---

* Utiliser les cookies pour maintenir l'information de session 
  * Totalement à la charge du client
    * Permet de répondre avec un autre serveur (répartition de charge)
  * Mais l'utilisateur peut manipuler l'information de session (=> crypter et signer)
  * Limité à une faible quantité de données ($\simeq$ 4000 octets)
 
%%% js
var cookieSession = require('cookie-session');
app.use(cookieSession({
  secret: 'mot-de-passe-du-cookie',
}));

app.get('/start-session/:user', (req, res) => {
  var user = req.params.user;
  req.session.user = req.params.user;
  res.send('Hello ' + req.params.user);
});

app.get('/end-session', (req, res) => {
  var user = req.session.user;
  req.session = null;
  res.send('Bye ' + user);
});
%%%

--- Session : localStorage ---

* Côté client : localStorage / sessionStorage...
  * Permet de garder de l'information entre les requêtes
  * Limité au même site web, limité en taille ($\simeq$5Mo) et dans le temps
  * Permet de conserver l'état sans passer par le serveur
  * Ne marche pas que dans le même navigateur 

%%% js
// Attention, côté client uniquement
var user = localStorage.getItem('user');
if (user) {
  console.log('Votre nom:', user);
} else {
  localStorage.setItem('user', "Bob l'éponge");
}
%%%

* Attention, les infos sauvegardées ne sont pas envoyées au serveur automatiquement

--- Session : Redis ---

* Côté serveur : une base de données "key/value"
  * En mémoire uniquement
  * Très rapide
  * Conjointement avec un cookie qui identifie la session

%%% js
var session = require('express-session');
var redis = require('redis');
var redisClient = redis.createClient();
var redisStore = require('connect-redis')(session);

app.use(session({
  secret: 'mot-de-passe-du-cookie',
  name: 'session',
  resave: false,
  saveUninitialized: true,
  cookie: { secure: false }, 
  store: new redisStore({ host: 'localhost', port: 6379, client: redisClient, ttl: 86400 }),
}));

app.get('/', (req, res) => {
  req.session.user = 'toto';
});
%%%

--- Attaque sur les sessions ---

* Problème : la session donne accès à des informations sensibles
  * Identifiant d'utilisateur dans la base de données 
  * Booléen d'authentification, droits, etc

* Un attaquant malicieux peut rejouer une requête HTTP
  * HTTP à la place de HTTPS : lecture de la communication en clair
  * XSS (cross-site scripting) : injection d'une balise `<script>` qui lit les cookies
  * Malware : un logiciel tiers lit les cookies du navigateur

* Un attaquant malicieux peut deviner l'information
  * Algorithme de génération trop simple (ex: identifiants consécutifs)
  * Réutilisation dans une autre requête

* Remédiation
  * Utiliser HTTPS
  * Options de cookies : Secure, HTTPOnly, SameSite...
  * Crypter et signer les cookies
  * Inclure une date de validité dans l'information de session


--- Gestion des utilisateurs ---

* Site avec utilisateur connecté
  * Nom
  * Mot de passe
  * Droits
  * Profil
  * État
  * ...

* Cycle de vie
  # Création
  # Login
  # Utilisation authentifiée
    * Accès à des pages / ressources privées
    * Récupération des données personnelles (GDPR)
  # Destruction

--- Authentification dans express ---

* Dans express, on peut stocker l'utilisateur dans une session
  # Un formulaire permet de saisir le nom et mot de passe de l'utilisateur
  # Le formulaire fait un appel POST vers /login
  # Le serveur met l'identifiant d'utilisateur dans la session
  # ...

%%% js
app.use(cookieSession(...));

app.post('/login', (req, res) => {
  const user = check_login(req.body.user, req.body.password);
  if(user != -1) {
    req.session.user = user;
    res.redirect('/login-success');
  } else {
    res.redirect('/login-failed');
  }
});

app.get('/logout', (req, res) => {
  req.session = null;
  res.redirect('/');
});
%%%

--- Authentification dans express (2) ---

* Une fois la gestion de l'authentification en place
  * On ajoute un middleware qui vérifie que l'utilisateur est bien authentifié

%%% js
// middleware
function is_authenticated(req, res, next) {
  if(req.session.user !== undefined) {
    return next();
  }
  res.status(401).send('Authentication required');
}

app.get('/not-valuable-stuff', (req, res) => {
  res.sendFile("les-résultats-de-l'om.txt");
});

app.get('/valuable-stuff', is_authenticated, (req, res) => {
  res.sendFile('tous-mes-bitcoin.txt');
});
%%%

--- Cryptage de mot de passe ---

* En général, on stocke les utilisateurs dans une base de données
  * Problème : que se passe-t-il si la base est compromise ?
  * Solution : crypter le mot de passe avant de le stocker

%%% js
var bcrypt = require('bcrypt');

function crypt_password(password) {
  var saved_hash = bcrypt.hashSync(password, 10);
  return saved_hash;
}

function compare_password = function(password, saved_hash) {
  return bcrypt.compareSync(password, saved_hash) == true;
};
%%%

* Lire la page \url{https://www.npmjs.com/package/bcrypt}
  * Considérations liées à la sécurité

--- Conclusion ---

* HTTP est un protocole sans état
  * Il faut s'occuper de maintenir la session

* Types de sessions
  * Cookie (envoyé à chaque requête)
  * BDD côté serveur
  * Côté client uniquement

* Permet d'implémenter l'authentification des utilisateurs
  * Express : sous forme de middleware


% vi:set spell spelllang=fr:
