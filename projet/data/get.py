#import wikipedia

#wikipedia.set_lang('fr')
#print(wikipedia.summary("Anarrhinum_bellidifolium", sentences=2))

import sys, json

#from mediawiki import MediaWiki
#wikipedia = MediaWiki(url='https://fr.vikidia.org/w/api.php')
#wikipedia = MediaWiki(lang='fr')

#p = wikipedia.page(sys.argv[1])
#print(p.summary)
#print(p.images)

names = [line.strip() for line in open("names.txt").readlines()]

final_data = []

for i, line in enumerate(sys.stdin):
    if line.strip() != "":
        data = json.loads(line.strip())
        if 'extract' in data and 'thumbnail' in data:
            final_data.append({
                'description': data['extract'],
                'thumbnail': data['thumbnail']['source'],
                'image': data['originalimage']['source'],
                'name': names[i]
            })
        
print(json.dumps(final_data, ensure_ascii=False, indent=' '))

