curl http://www.plantes-sauvages.com/user/plante/index.php| grep "index.php?id=" | cut -f2 -d'"'| awk '{print "http://www.plantes-sauvages.com/user/plante/"$0}' > urls.txt
#curl http://www.plantes-sauvages.com/user/plante/index.php| grep "index.php?id=" | cut -f3 -d '>' | cut -f1 -d'<' > names.txt

rm plantes.jsonl
cat urls.txt | while read
do
  url=$REPLY
  page=`curl -s "$url" | grep -o 'https://fr.wikipedia.org/[^\"]*' | grep -v '<' | cut -f5 -d/`
  if [ "$page" != "" ]; then
    #echo URL $url
    curl -s "https://fr.wikipedia.org/api/rest_v1/page/summary/$page" >> plantes.jsonl
  fi
  echo >> plantes.jsonl
done

