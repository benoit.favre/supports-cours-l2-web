let Sqlite = require('better-sqlite3');

let db = new Sqlite('db.sqlite');

// user
function add_user(name, password) {
  if(!name || !password) return -1;
  try {
    let insert = db.prepare('INSERT INTO users VALUES (?, ?)');
    let result = insert.run([name, password]);
    return result.lastInsertRowid;
  } catch(e) {
    if(e.code == 'SQLITE_CONSTRAINT_PRIMARYKEY') return -1;
    throw e;
  }
}

function get_user_name(id) {
  let select = db.prepare('SELECT name FROM users WHERE rowid = ?');
  let result = select.get([id]);
  if(result) return result.name;
  return null;
}

function login(name, password) {
  let select = db.prepare('SELECT rowid FROM users WHERE name = ? AND password = ?');
  let result = select.get([name, password]);
  if(result) return result.rowid;
  return -1;
}

// messages
function add_message(author, text) {
  let insert = db.prepare('INSERT INTO messages VALUES (?, ?, ?)');
  insert.run([text, new Date().toString(), author]);
}

function list_messages() {
  let select = db.prepare('SELECT m.text, m.date, u.name as author FROM messages m INNER JOIN users u ON u.rowid = m.author ORDER BY m.rowid DESC LIMIT 10')
  let result = select.all();
  return result.reverse();
}

module.exports = {
  add_user: add_user,
  get_user_name: get_user_name,
  login: login,
  add_message: add_message,
  list_messages: list_messages,
};
