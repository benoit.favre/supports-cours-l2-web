const model = require('./model_sqlite');

// setup router
const express = require('express'); 
const app = express();

// parse form arguments in POST requests
const bodyParser = require('body-parser');
app.use(bodyParser.urlencoded({ extended: false }));

// setup session handler
const cookieSession = require('cookie-session');
app.use(cookieSession({
  secret: 'mot-de-passe-du-cookie',
}))

// setup template engine
const cons = require('consolidate'); // template engine
app.engine('html', cons.mustache);
app.set('view engine', 'html');
app.set('views', __dirname + '/views_bootstrap');

// setup view generator
const view = require('./view_templates');

// handle static pages
app.use(express.static('public'));

// middleware to render info
function add_info(req, res, next) {
  const info = {
    'not-authenticated': {type: 'danger', text: "Erreur : cette page requiert de se connecter"},
    'invalid-credentials': {type: 'danger', text: "Erreur : login ou mot de passe invalide"},
    'invalid-user': {type: 'danger', text: "Erreur : l'utilisateur existe déjà ou est invalide"},
    'password-mismatch': {type: 'danger', text: "Erreur : les mots de passe ne concordent pas"},
    'logged-in': {type: 'info', text: "Info : utilisateur connecté"},
    'logged-out': {type: 'info', text: "Info : utilisateur déconnecté"},
  }
  if(req.query.info && req.query.info in info) {
    res.locals.info = info[req.query.info];
  }
  return next();
}
app.use(add_info);

// middleware to check if user was authenticated
function is_authenticated(req, res, next) {
  if(req.session.user) {
    return next();
  }
  res.status(401).send('Authentication required');
}

app.get('/', (req, res) => {
  view.render(res, 'index', {});
});

app.post('/login', (req, res) => {
  if(!('password' in req.body && 'user' in req.body)) {
    res.status(400).send('invalid request');
    return;
  }
  const user = model.login(req.body.user, req.body.password);
  if(user != -1) {
    req.session.user = user;
    res.redirect('/messages?info=logged-in');
  } else {
    res.redirect('/?info=invalid-credentials');
  }
});

app.post('/new_user', (req, res) => {
  if(!('password' in req.body && 'confirm_password' in req.body && 'user' in req.body)) {
    res.status(400).send('invalid request');
    return;
  }
  if(req.body.password != req.body.confirm_password) {
    res.redirect('/?info=password-mismatch');
  } else {
    const user = model.add_user(req.body.user, req.body.password);
    if(user != -1) {
      req.session.user = user;
      res.redirect('/messages?info=logged-in');
    } else {
      res.redirect('/?info=invalid-user');
    }
  }
});

app.get('/messages_content', is_authenticated, (req, res) => {
  view.render(res, 'messages_content', {messages: model.list_messages()});
});

app.get('/messages', is_authenticated, (req, res) => {
  const data = {
    name: model.get_user_name(req.session.user),
    messages: model.list_messages(),
  }
  view.render(res, 'messages', data);
});

app.post('/add_message', (req, res) => {
  if(!('text' in req.body)) {
    res.status(400).send('invalid request');
    return;
  }
  model.add_message(req.session.user, req.body.text);
  res.redirect('/messages');
});

app.post('/logout', is_authenticated, (req, res) => {
  req.session = null;
  res.redirect('/?info=logged-out')
});

// create http server with self-generated certificate
const fs = require('fs');
const https = require('https');

const port = 3000;
const options = {
  key: fs.readFileSync('ssl/localhost.key'),
  cert: fs.readFileSync('ssl/localhost.crt')
};

const server = https.createServer(options, app);
server.listen(port, () => console.log(`https server on port ${port}!`));

