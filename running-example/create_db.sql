/* 
$ sqlite3 db.sqlite < create_db.sql
*/

DROP TABLE IF EXISTS users;
DROP TABLE IF EXISTS messages;

CREATE TABLE users (name TEXT PRIMARY KEY, password TEXT);
CREATE TABLE messages (text TEXT, date DATE, author ID);
