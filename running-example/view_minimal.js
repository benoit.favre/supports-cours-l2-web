function format_info(info) {
  if(info) return info;
  return '';
}

function format_messages(messages) {
  let content = '';
  for(message of messages) {
   content += '<p>' + message.author + ' : ' + message.text + '</p>'
  }
  return content;
}

function render(res, page, data) {
  console.log(data);

  let body;
  if(page == 'index') {

    body=`<p> ${format_info(data.info)} </p>

<form action="/login" method="get">
  <input type="text" name="user" placeholder="Nom">
  <input type="password" name="password" placeholder="Mot de passe">
  <input type="submit" value="Se connecter">
</form>

<form action="/new_user" method="get">
  <input type="text" name="user" placeholder="Nom">
  <input type="password" name="password" placeholder="Mot de passe">
  <input type="password" name="confirm_password" placeholder="Confirmer">
  <input type="submit" value="Nouvel utilisateur">
</form>`;

  } else if(page == 'messages') {

    body=`<p> ${format_info(data.info)} </p>
${format_messages(data.messages)}
<form action="/add_message" method="get">
  <input type="hidden" name="user" value="${data.user}">
  <input type="text" name="text" placeholder="Texte" autofocus>
  <input type="submit" value="Ajouter">
</form>

<form action="/logout" method="post">
  <input type="hidden" name="user" value="${data.user}">
  <input type="submit" value="Se déconnecter">
</form>`;

  }

  let content = `<!DOCTYPE html>
<head>
  <meta charset="utf-8">
</head>
<body>
  ${body}
</body>
</html>`;

  res.writeHead(200, 'Content-Type', 'text/html')
  res.end(content);
}

module.exports = {
  render: render,
};
