Logiciels requis
----------------

node (https://nodejs.org)
npm (https://www.npmjs.com)
sqlite3 (https://sqlite.org)
openssl (https://www.openssl.org)

Installation
------------

Installer les modules node :
$ npm install

Créer un certificat SSL :
$ ./ssl/generate-localhost-ssl-certs.sh 

Ajouter l'autorité de certification ssl/localhost-ca.crt à votre navigateur (ou une exception au certificat invalide)
- Firefox : préférences / voir les certificats / importer
- Chrome : paramètres / gérer les certificats / importer

Créer la base de données :
$ sqlite3 db.sqlite < create_db.sql

Lancement
---------

$ node server_express.js

Aller dans le navigateur à l'adresse https://localhost:3000

Protocole
---------

(Presque) tout se passe côté serveur. Le client remplit des formulaires qui génèrent des requêtes POST.

Le serveur est constitué de trois pages (deux pages + une partielle) :
GET /index qui contient le formulaire de connexion et celui d'ajout d'utilisateur
GET /messages qui affiche la liste des messages, permet d'en écrire, et de se déconnecter
GET /messages_content qui retourne que le contenu de la div #messages pour rafraichir la liste des messages

Il répond aussi aux url de manipulation des données suivantes :
POST /login (user, password) pour se connecter
POST /new_user (user, password, confirm_password) pour ajouter un utilisateur
POST /add_message (text) pour ajouter un message
POST /logout () pour se déconnecter
Ces url résultent toujours en une redirection vers une des pages affichables.

L'authentification est maintenue dans un cookie de session crypté (session.user).
Les pages /messages, /messages_content, /add_message, /logout requirent d'être authentifié.

Un message peut être passé lors d'une redirection par l'intermédiaire du paramètre "info" dans la requête.
Idéalement, ces messages pourraient être mis dans le cookie crypté.

Le seul script côté client est celui qui rafraichit la div contenant les messages toutes les secondes.

Les données sont gérées par le modèle. Il y a deux tables : users et messages. 
La première contient les noms des utilisateurs et leurs mots de passe, sqlite ajoutant un champ rowid qui sert de numéro d'utilisateur. Le nom d'utilisateur doit être unique.
La seconde table contient pour chaque message son texte, sa date d'ajout et le numéro de l'utilisateur l'ayant écrit.

Variantes
---------

* Changer le serveur
  Lancer `node server_minimal.js`
  Ce serveur n'utilise pas de routage et fait passer les informations dans la requete (GET)
  Il a un générateur de vue associé (view_minimal.js) et permet de bien comprendre le protocole.

* Changer le modèle de données
  => changer la ligne `model = require('./model_sqlite');`
  model_arrays.js (structures de données js)
  model_sqlite_insecure.js (base de données vulnérable aux injections sql)
  model_sqlite.js (base de données sqlite)

* Changer la vue
  => changer la ligne `view = require('./view_templates');`
  views_generated.js (html généré par la fonction) 
  views_templates.js (utilise des templates mustache, choisir le rendu ci-après)

  => changer le répertoire à la ligne `app.set('views', __dirname + '/views_bootstrap');`
  views_basic (template mustache en html basic)
  views_css (+css)
  views_bootstrap (repose sur bootstrap)

