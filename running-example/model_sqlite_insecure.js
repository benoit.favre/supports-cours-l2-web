/* see sql injection examples herein */ 
let Sqlite = require('better-sqlite3');

let db = new Sqlite('db.sqlite');

// user
function add_user(name, password) {
  if(!name || !password) return -1;
  try {
    let insert = db.prepare(`INSERT INTO users VALUES ("${name}", "${password}")`);
    let result = insert.run();
    return result.lastInsertRowid;
  } catch(e) {
    if(e.code == 'SQLITE_CONSTRAINT_PRIMARYKEY') return -1;
    throw e;
  }
}

function get_user_name(id) {
  let select = db.prepare(`SELECT name FROM users WHERE rowid = "${id}"`);
  let result = select.get();
  if(result) return result.name;
  return null;
}

function login(name, password) {
  // SQL injection attack with name = '"benoit"--'
  // SQL injection attack with password = '" or ""="'
  let select = db.prepare(`SELECT rowid FROM users WHERE name = "${name}" AND password = "${password}"`);
  console.log(select);
  let result = select.get();
  if(result) return result.rowid;
  return -1;
}

// messages
function add_message(author, text) {
  // SQL injection, attack with message = '" || (SELECT password FROM USERS WHERE name="benoit"), "", 1)--"'
  let insert = db.prepare(`INSERT INTO messages VALUES ("${text}", "${new Date()}", "${author}")`);
  console.log(insert);
  insert.run();
}

function list_messages() {
  let select = db.prepare('SELECT m.text, m.date, u.name as author FROM messages m INNER JOIN users u ON u.rowid = m.author ORDER BY m.rowid DESC LIMIT 10')
  let result = select.all();
  return result.reverse();
}

module.exports = {
  add_user: add_user,
  get_user_name: get_user_name,
  login: login,
  add_message: add_message,
  list_messages: list_messages,
};
