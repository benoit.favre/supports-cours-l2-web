function format_info(data) {
  if('info' in data) {
    return data.info.text;
  }
  return '';
}

function format_messages(messages) {
  let content = '';
  for(message of messages) {
   content += '<p> <strong>' + message.author + '</strong> : ' + message.text + '</p>'
  }
  return content;
}

function render(res, page, data) {
  let body;
  if(page == 'index') {

    body=`<h1> Bienvenue sur le serveur de tchatche </h1>

<p> ${format_info(res.locals)} </p>

<h2> Se connecter </h2>
<form action="/login" method="post">
  <input type="text" name="user" placeholder="Nom">
  <input type="password" name="password" placeholder="Mot de passe">
  <input type="submit" value="Se connecter">
</form>

<h2> Créer un utilisateur </h2>
<form action="/new_user" method="post">
  <input type="text" name="user" placeholder="Nom">
  <input type="password" name="password" placeholder="Mot de passe">
  <input type="password" name="confirm_password" placeholder="Confirmer">
  <input type="submit" value="Nouvel utilisateur">
</form>`;

  } else if(page == 'messages') {

    body=`<h1> Liste des messages </h1>

<p> ${format_info(res.locals)} </p>

<div id="messages">
  ${format_messages(data.messages)}
</div>

<h2> Ajouter un message </h2>
<form action="/add_message" method="post">
  <input type="text" name="text" placeholder="Texte" autofocus>
  <input type="submit" value="Ajouter">
</form>

<form action="/logout" method="post">
  Bienvenue ${data.name}
  <input type="submit" value="Se déconnecter">
</form>`;

  }

  let content = `<!DOCTYPE html>
<head>
  <meta charset="utf-8">
</head>
<body>
  ${body}
</body>
</html>`;

  res.set('Content-Type', 'text/html')
  res.send(content);
}

module.exports = {
  render: render,
};
