#!/usr/bin/env bash
# Based on https://stackoverflow.com/questions/49553138/how-to-make-browser-trust-localhost-ssl-certificate

dir=`dirname "$0"`
cd "$dir"

set -eu
org=localhost-ca
domain=localhost

openssl genpkey -algorithm RSA -out "$org".key
openssl req -x509 -key "$org".key -out "$org".crt \
    -subj "/CN=$org/O=$org"

openssl genpkey -algorithm RSA -out "$domain".key
openssl req -new -key "$domain".key -out "$domain".csr \
    -subj "/CN=$domain/O=$org"

openssl x509 -req -in "$domain".csr -days 365 -out "$domain".crt \
    -CA "$org".crt -CAkey "$org".key -CAcreateserial \
    -extfile <(cat <<END
basicConstraints = CA:FALSE
subjectKeyIdentifier = hash
authorityKeyIdentifier = keyid,issuer
subjectAltName = DNS:$domain
END
)

echo 
echo "Note: you need to add $dir/$org.crt as trusted authority in your browser and restart it."
echo "You can then use $dir/$domain.{key,crt} as ssl key and certificate in your webserver."
