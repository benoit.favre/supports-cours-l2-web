const model = require('./model_sqlite');
const view = require('./view_minimal');

const http = require('http');
const url_parser = require('url');

function redirect(res, location) {
  res.writeHead(302, {'Location': location});
}

let server = http.createServer((req, res) => {
  let url = url_parser.parse(req.url, true);

  if(url.pathname == '/') {
    view.render(res, 'index', {info: url.query.info});

  } else if(url.pathname == '/login') {
    const user = model.login(url.query.user, url.query.password);
    if(user != -1) {
      redirect(res, '/messages?info=logged-in&user=' + user);
    } else {
      redirect(res, '/?info=invalid-credentials');
    }

  } else if(url.pathname == '/new_user') {
    if(url.query.password != url.query.confirm_password) {
      redirect(res, '/?info=password-mismatch');
    } else {
      const user = model.add_user(url.query.user, url.query.password);
      if(user != -1) {
        redirect(res, '/messages?info=logged-in&user=' + url.query.user);
      } else {
        redirect(res, '/?info=invalid-user');
      }
    }

  } else if(url.pathname == '/messages') {
    const data = {
      user: url.query.user,
      name: model.get_user_name(url.query.user),
      messages: model.list_messages(),
      info: url.query.info,
    }
    view.render(res, 'messages', data);

  } else if(url.pathname == '/add_message') {
    model.add_message(url.query.user, url.query.text);
    redirect(res, '/messages?user=' + url.query.user);

  } else if(url.pathname == '/logout') {
    redirect(res, '/?info=logged-out');

  }
  res.end();
});

const port = 3000;
server.listen(port, () => console.log(`http server on port ${port}!`));

