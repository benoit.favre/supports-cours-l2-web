
app.get('/', (req, res) => {
  let country = req.query.country;
  sql.connect((err) => {
    sql.query('SELECT * FROM countries WHERE name = "${country}"', (err, res) => {
      res.send(result);
    });
  });
});

app.get('/', (req, res) => {
  let country = req.query.country;
  sql.connect((err) => {
    sql.query('SELECT * FROM countries WHERE name = ?', [country], (err, res) => {
      res.send(result);
    });
  });
});

sql.query('CREATE TABLE countries (name TEXT, production FLOAT, consumption FLOAT, surface FLOAT, population INT)');

app.get('/', (req, res) => {
  sql.query('SELECT name, population FROM countries WHERE surface > 146', (err, rows) => {
    let table = '<table>';
    for(i in rows) {
      table += `<tr><td>${rows[i].name}</td><td>${rows[i].population}</td></tr>`;
    }
    table += '</table>';
    res.send(table);
  });
});

app.get('/add-or-modify', (req, res) => {
  let country = req.query.country;
  sql.query('SELECT * FROM countries WHERE name = ?', [country], (err, rows) => {
    res.send(`
  <form action="/add-or-modify" method="post">
    <input type="text" name="name" value="${rows[0].name}">
    <input type="text" name="production" value="${rows[0].production}">
    <input type="submit">
  </form>
    `);
  });
}
app.post('/add-or-modify', (req, res) => {
  sql.query('SELECT * FROM countries WHERE name = ?', [req.body.name], (err, rows) => {
    if(rows.length == 0) {
      sql.query('INSERT INTO countries (name, production) VALUES (?, ?)', [req.body.name, req.body.production], (err, rows) => {
        res.send('ok');
      });
    } else {
      sql.query('UPDATE countries SET production = ? WHERE name = ?', [req.body.production, req.body.name], (err, rows) => {
        res.send('ok');
      });
    }
  });
});

