var http = require('http');

function gen_html(messages) {
  let result = '<ul>';
  for(message of messages) result += '<li>' + message + '</li>';
  return result + '</ul>';
}

function add_message(messages, text) {
  messages.push(text);
}

function remove_message(messages, index) {
  messages.splice(index, 1);
}

var messages = ["abc", "def"];

http.createServer(function (req, res) {
  if(req.method == 'POST') {
    add_message(messages, unescape(req.url).substring(1));
  } else if (req.method == 'DELETE') {
    remove_message(messages, parseInt(req.url.substring(1)));
  } else if (req.method == 'GET') {
    res.writeHead(200, {'Content-Type': 'text/html'});
    res.end(gen_html(messages));
  }
  res.end();
}).listen(1337, '127.0.0.1');
