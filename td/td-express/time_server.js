let express = require('express');

let app = express();

app.get('/time', (req, res) => {
  let time = new Date().toString();
  res.end(time);
});

app.get('/', (req, res) => {
  let client = `<h1 id="time"></h1><script>
setInterval(async function() {
  let response = await fetch('/time');
  let time = await response.text();
  document.getElementById('time').innerText = time;
}, 1000);
</script>`;
  res.send(client);
});

app.listen(3000);
