let express = require('express');
let app = express();

let data = {cat: 'felix', dog: 'rantanplan'};

app.get('/all_keys', (req, res) => {
  for (key in data) {
    res.write(key + '\n');
  }
  res.end();
});

app.get('/read/:key', (req, res) => {
  let key = req.params.key;
  if (key in data) {
    res.end(data[key].toString());
  } else {
    res.end(404, 'Not found');
  }
});

app.get('/write/:key/:value', (req, res) => {
  let key = req.params.key;
  let value = req.params.value;
  data[key] = value;
  res.end();
});

app.listen(3000);
