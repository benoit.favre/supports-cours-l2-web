let express = require('express');
let app = express();
app.get('/echo/:message', (req, res) => {
  let message = req.params.message;
  let echo = '';
  for (let i = 0; i < message.length; i++) {
    let c = message.charAt(i);
    if (c >= 'A' && c <= 'Z') c = c.toLowerCase();
    else if (c >= 'a' && c <= 'z') c = '.';
    else if (c == '.') c = ' ';
    echo += c;
  }
  res.send(echo);
});
app.listen(3000);

