let express = require('express');
let app = express();

let rate = 0.871523;

app.get('/', (req, res) => {
  let html = `<div>Taux de change ${luminycoin.rate()}</div>
  <form action="/exchange" method="GET">
    <input type="text" name="amount"> 
    <select name="currency">
      <option value="euro">Euro</option>
      <option value="lyc">LyC</option>
    </select>
    <input type="submit" value="Changer">
  </form>`;
  res.send(html);
});

function check_inputs(req) {
  if (!('currency' in req.query)) return false;
  if (!(req.query.currency == 'euro' || req.query.currency == 'lyc')) return false;
  if (!('amount' in req.query)) return false;
  if (isNaN(parseFloat(req.query.amount))) return false;
  return true;
}

app.get('/exchange', (req, res) => {
  if (check_inputs(req)) {
    let currency = req.query.currency;
    let amount = parseFloat(req.query.amount);
    if (currency == 'euro') {
      let value = amount * rate;
      res.send(value + ' lyc');
    } else if (currency == 'lyc') {
      let value = amount / rate;
      res.send(value + ' euro');
    }
  } else {
    res.status(400).send('Bad request');
  }
});

app.listen(3000);
