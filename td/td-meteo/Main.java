import java.io.*;

class Main {
  public static String generateHeader(String title) {
  	return "<!DOCTYPE html>\n<html>\n<head>\n<title>" + title + "</title>\n</head>"
  		+ "<style>"
  		+ ".very-cold { background: DodgerBlue; }"
  		+ ".cold { background: LimeGreen; }"
  		+ ".warm { background: Gold; }"
  		+ ".hot { background: Orange; }"
  		+ ".very-hot { background: OrangeRed; }"
  		+ "</style>"
  		+ "\n<body>";
  }
  
  public static String generateTableRow(String[] cells, boolean isTemperature) {
  	String result = "<tr>\n";
  	boolean isFirst = true;
  	
  	for(String cell: cells) {
  		if(!isFirst && isTemperature) {
	  		int temperature = Integer.parseInt(cell);
	  		
	  		String cls = "";
	  		if(temperature <= 0) cls = "very-cold";
	  		else if(temperature <= 10) cls = "cold";
	  		else if(temperature <= 20) cls = "warm";
	  		else if(temperature <= 30) cls = "hot";
	  		else if(temperature < 1000) cls = "very-hot";
	  		
	  		result += "  <td class=" + cls + ">" + cell + "</td>\n";
  		} else {
  			result += "  <td>" + cell + "</td>\n";
  		}
  		isFirst = false;
  	}
  	return result + "</tr>\n";
  }
  
  public static String[] loadOneYear(BufferedReader reader) throws IOException {
  	String[] cells = new String[13];
  	for(int i = 0; i < 12; i++) {
  		String line = reader.readLine();
  		String[] parts = line.split("\t");
  		cells[i + 1] = parts[2];
  		if(i == 0) cells[0] = parts[0];
  	}
  	return cells;
  }
  
  public static String generateTable(String filename) throws IOException {
  	String result = "<table>\n";
  	String[] header = {"Année", "J", "F", "M", "A", "M", "J", "J", "A", "S", "O", "N", "D"};
  	result += generateTableRow(header, false);
  	
  	BufferedReader reader = new BufferedReader(new FileReader(filename));
  	
  	for(int i = 0; i < 100; i++) {
  		String[] cells = loadOneYear(reader);
  		result += generateTableRow(cells, true);
  	}
  	
  	return result + "</table>";
  }
  
  public static String generateFooter() {
  	return "</body>\n</html>";
  }
  
  public static void main(String args[]) throws IOException {
  	System.out.println(generateHeader("Météo"));
  	System.out.println(generateTable("meteo.data"));
    System.out.println(generateFooter());
  }
}
