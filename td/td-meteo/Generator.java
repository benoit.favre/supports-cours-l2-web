import java.io.*;

public class Generator {
  public static void main(String[] args) throws IOException {
    System.out.print(
        "<!DOCTYPE html>\n"
      + "<html>\n"
      + "  <head>\n"
      + "    <meta charset=\"utf-8\">\n"
      + "    <style>\n"
      + "      td, tr {text-align: center}\n"
      + "      .very-cold {background: DodgerBlue}\n"
      + "      .cold {background: limegreen}\n"
      + "      .normal {background: gold}\n"
      + "      .hot {background: orange}\n"
      + "      .very-hot {background: orangered}\n"
      + "    </style>\n"
      + "  </head>\n"
      + "  <body>\n"
      + "    <table>\n"
      + "      <thead><tr><th>Année</th><th>J</th><th>F</th><th>M</th><th>A</th><th>M</th><th>J</th><th>J</th><th>A</th><th>S</th><th>O</th><th>N</th><th>D</th></tr></thead>\n"
    );
    BufferedReader reader = new BufferedReader(new FileReader("meteo.data"));
    String line;
    int i = 1;
    while(null != (line = reader.readLine())) {
      String tokens[] = line.split("\t");

      int temperature = Integer.parseInt(tokens[2]);
      String tempClass;
      if(temperature <= 0) tempClass = "very-cold";
      else if(temperature <= 10) tempClass = "cold";
      else if(temperature <= 20) tempClass = "normal";
      else if(temperature <= 30) tempClass = "hot";
      else tempClass = "very-hot";

      if(tokens[1].equals("1")) {
        System.out.print("      <tr><td>" + tokens[0] + "</td><td class=" + tempClass + ">" + tokens[2] + "</td>");
      } else if(tokens[1].equals("12")) {
        System.out.print("<td class=" + tempClass + ">" + tokens[2] + "</td></tr>\n");
      } else {
        System.out.print("<td class=" + tempClass + ">" + tokens[2] + "</td>");
      }
    }
    System.out.print(
        "    </table>\n"
      + "  </body>\n"
      + "</html>\n"
    );
  }
}
